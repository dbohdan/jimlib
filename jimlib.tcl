#! /usr/bin/env jimsh
# A self-contained utility command library for Jim Tcl that fills some gaps.
# ==============================================================================
# This code with the exception of the following procedures
# * [lib::commands]
# * [lib::try]
# is copyright (c) 2019-2024 D. Bohdan and contributors listed in
# AUTHORS and is released under the MIT license (below).
# ==============================================================================
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# ==============================================================================
# The procedure [lib::commands] is copyright (c) 2013-2019 Poor Yorick and is
# derived from the original 1998 implementation of [cmdSplit] by
# Donald G. Porter, which is in the public domain as a US government work.
# It is released under the Tcl license (below).
# ==============================================================================
# This software is copyrighted by the Regents of the University of
# California, Sun Microsystems, Inc., Scriptics Corporation, ActiveState
# Corporation and other parties.  The following terms apply to all files
# associated with the software unless explicitly disclaimed in
# individual files.
#
# The authors hereby grant permission to use, copy, modify, distribute,
# and license this software and its documentation for any purpose, provided
# that existing copyright notices are retained in all copies and that this
# notice is included verbatim in any distributions. No written agreement,
# license, or royalty fee is required for any of the authorized uses.
# Modifications to this software may be copyrighted by their authors
# and need not follow the licensing terms described here, provided that
# the new terms are clearly indicated on the first page of each file where
# they apply.
#
# IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
# DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
# IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
# NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
# MODIFICATIONS.
#
# GOVERNMENT USE: If you are acquiring this software on behalf of the
# U.S. government, the Government shall have only "Restricted Rights"
# in the software and related documentation as defined in the Federal
# Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2).  If you
# are acquiring the software on behalf of the Department of Defense, the
# software shall be classified as "Commercial Computer Software" and the
# Government shall have only "Restricted Rights" as defined in Clause
# 252.227-7014 (b) (3) of DFARs.  Notwithstanding the foregoing, the
# authors grant the U.S. Government and others acting in its behalf
# permission to use and distribute the software in accordance with the
# terms specified in this license.
# ==============================================================================
# The procedure [lib::try] is adapted from Tcllib 1.19 code that carries the
# following copyright notice.
# ==============================================================================
# (C) 2008-2011 Donal K. Fellows, Andreas Kupries, BSD licensed.
# ==============================================================================

namespace eval lib {
    variable bytelength 1
    try {
        string bytelength {}
    } on error _ {
        set bytelength 0
        unset _
    }

    variable jim [expr {
        [info exists ::tcl_platform(engine)]
        && $::tcl_platform(engine) eq {Jim}
    }]

    variable version 0.16.0
}


proc lib::$ {varName args} {
    upvar 1 $varName v
    if {![info exists v]} {
        return -code error \
               -errorcode {JIMLIB $ NO-SUCH-VARIABLE} \
               "can't read \"$varName\": no such variable"
    }

    if {[llength $args] == 0} {
        return $v
    } else {
        return [dict get $v {*}$args]
    }
}


proc lib::arity {expr arguments {command {}}} {
    if {$command eq {}} {
        set command [lindex [info level -1] 0]
    }

    if {$arguments ne {}} {
        set arguments " $arguments"
    }

    if {![uplevel 1 [list expr $expr]]} {
        return -code error \
               -errorcode {TCL WRONGARGS} \
               "wrong # args: should be \"$command$arguments\""
    }
}


namespace eval lib::checksum {}
namespace eval lib::checksum::parse {}

proc lib::checksum::parse::many text {
    concat {*}[lmap line [split $text \n] {
        if {[regexp {^\s*$} $line]} continue

        set line [string trimright $line \r]

        try {
            tagged-line $line
        } on error _ {
            try {
                line $line
            }
        }
    }]
}


proc lib::checksum::parse::line line {
    if {![regexp {^(\\?)([0-9a-fA-Z]+)\s+\*?(.*)$} $line _ slash sum path]} {
        return -code error \
               -errorcode {JIMLIB CHECKSUM PARSE LINE} \
               [list can't parse line $line]
    }

    if {$slash eq "\\"} {
        set path [subst -nocommands -novariables $path]
    }

    list ? $path $sum
}


proc lib::checksum::parse::tagged-line line {
    if {![regexp -indices {^(\\?)([^\s]+)\s+\(} $line \
                          beforeInd slashInd algoInd]} {
        return -code error \
               -errorcode {JIMLIB CHECKSUM PARSE TAGGED-LINE} \
               [list can't parse line $line]
    }

    set algo [string range $line {*}$algoInd]

    if {![regexp -indices {\)\s+=\s+([0-9a-fA-Z]+)$} $line \
                          afterInd sumInd]} {
        return -code error \
               -errorcode {JIMLIB CHECKSUM PARSE TAGGED-LINE} \
               [list can't parse line $line]
    }

    set sum [string range $line {*}$sumInd]
    set path [string range $line \
                           [lindex $beforeInd 1]+1 \
                           [lindex $afterInd 0]-1]

    if {$slashInd eq {0 0}} {
        set path [subst -nocommands -novariables $path]
    }

    list $algo $path $sum
}


namespace eval lib::clipboard {}

proc lib::clipboard::copy s {
    wait [exec xclip -in -selection clipboard << $s &]
    return
}


proc lib::clipboard::paste {} {
    exec xclip -out -selection clipboard
}


namespace eval lib::clock {}

# A rough implementation of [clock add]. Doesn't understand time zones, leap
# seconds, and other nuance.
proc lib::clock::add {clockval args} {
    foreach {amount unit} $args {
        set mul 0

        switch $unit {
            s -
            second -
            seconds {
                set mul 1
            }

            m -
            min -
            mins -
            minute -
            minutes {
                set mul 60
            }

            h -
            hr -
            hrs -
            hour -
            hours {
                set mul $(60 * 60)
            }

            d -
            day -
            days {
                set mul $(60 * 60 * 24)
            }

            w -
            wk -
            wks -
            week -
            weeks {
                set mul $(60 * 60 * 24 * 7)
            }

            mo -
            mos -
            mon -
            mons -
            month -
            months {
                set clockval [add-months $clockval $amount]
            }

            y -
            yr -
            yrs -
            year -
            years {
                set clockval [add-months $clockval $(12 * $amount)]
            }

            default {
                return -code error \
                       -errorcode {JIMLIB CLOCK ADD} \
                       [list unknown time unit $unit]
            }
        }

        set clockval $($clockval + $amount * $mul)
    }

    return $clockval
}


proc lib::clock::add-months {clockval n} {
    set daysInMonth {xx 31 2x 31 30 31 30 31 31 30 31 30 31}

    set sign $( $n < 0 ? -1 : 1 )
    set n $( $sign * $n )

    for {set i 0} {$i < $n} {incr i} {
        lassign [clock format $clockval -format {%Y %m} -gmt 1] year month
        if {$sign == -1} {
            lassign [prev-month $year $month] year month
        }

        set leap [leap-year? $year]
        lset daysInMonth 2 $($leap ? 29 : 28)

        set clockval $(
            $clockval + $sign * [lindex $daysInMonth $month] * 24 * 60 * 60
        )
    }

    return $clockval
}


proc lib::clock::leap-year? year {
    if {$year % 4 != 0} {
        return 0
    }

    if {$year % 100 != 0} {
        return 1
    }

    if {$year % 400 != 0} {
        return 0
    }

    return 1
}


proc lib::clock::prev-month {year month} {
    incr month -1

    if {$month < 1} {
        incr year -1
        set month 12
    }

    return [list $year $month]
}


proc lib::clock::scan-incremental {date {options {}} {debug 0}} {
    set date [regsub -all {[ :.T/]+} $date {-}]

    set resultTimeVal {}
    set resultFormat {}
    foreach {formatScan formatStandard padding} {
        {%Y}                {%Y}                {-01-01-00-00-00}
        {%Y-%m}             {%Y-%m}             {-01-00-00-00}
        {%Y-%m-%d}          {%Y-%m-%d}          {-00-00-00}
        {%Y-%m-%d-%H-%M}    {%Y-%m-%dT%H:%M}    {-00}
        {%Y-%m-%d-%H-%M-%S} {%Y-%m-%dT%H:%M:%S} {}
    } {
        if {$debug} {
            log debug [list $formatScan $date]
        }
        try {
            clock scan $date -format $formatScan {*}$options
        } on ok scan {
            # Work around unexpected treatment of %Y and %Y-%m dates;
            # see http://wiki.tcl-lang.org/2525.
            set resultTimeVal [clock scan [join [list $date $padding] ""] \
                    -format {%Y-%m-%d-%H-%M-%S} {*}$options]
            set resultFormat $formatStandard
            if {$debug} {
                log debug match
                log debug [clock format $scan {*}$options]
            }
        } on error {} {}
    }
    return [list $resultTimeVal $resultFormat]
}


proc lib::configure {} {
    upvar 1 args args \
            config config

    if {[lindex $args 0] eq {-configure}} {
        set args [lassign $args _ configUpdate]

        set unknownKeys [lsort [set::diff [dict keys $configUpdate] \
                                          [dict keys $config]]]
        if {$unknownKeys ne {}} {
            return -code error \
                   -errorcode {JIMLIB CONFIGURE UNKNOWN-KEYS} \
                   [list unknown config keys $unknownKeys]
        }

        set config [dict merge $config $configUpdate]
    }
}


# By Donald G. Porter and Poor Yorick (https://wiki.tcl-lang.org/page/cmdSplit).
# Modified to return $report.
proc lib::commands {script {returnReport 0}} {
    set report {}
    set commands {}
    set command {}
    set comment 0
    set lineidx 0
    set offset 0

    foreach line [split $script \n] {
        set parts [split $line \;]
        set numparts [llength $parts]
        set partidx 0
        while 1 {
            set parts [lassign $parts[set parts {}] part]
            if {[string length $command]} {
                if {$partidx} {
                    append command \;$part
                } else {
                    append command \n$part
                }
            } else {
                set partlength [string length $part]
                set command [string trimleft $part[set part {}] "\f\n\r\t\v "]
                incr offset [expr {$partlength - [string length $command]}]
                if {[string match #* $command]} {
                    set comment 1
                }
            }

            if {$command eq {}} {
                incr offset
            } elseif {(!$comment || (
                    $comment && (!$numparts || ![llength $parts])))
                && [info complete $command\n]} {

                lappend commands $command
                set info [dict create character $offset line $lineidx]
                set offset [expr {$offset + [string length $command] + 1}]
                lappend report $info
                set command {}
                set comment 0
                set info {}
            }

            incr partidx
            if {![llength $parts]} break
        }
    }

    incr lineidx

    if {$command ne {}} {
        return -code error \
               -errorcode {JIMLIB COMMANDS INCOMPLETE} \
               [list {incomplete command} $command]
    }

    if {$returnReport} {
        return [list $commands $report]
    } else {
        return $commands
    }
}


namespace eval lib::csv {}

proc lib::csv::split {line {seps ,}} {
    if {$line eq {}} {
        return {}
    }

    set field {}
    set fields {}
    set prev {}
    set quoted 0

    foreach c [::split $line {}] {
        if {$c eq {"}} {
            if {$prev eq {"}} {
                append field \"
            }

            set quoted [expr {!$quoted}]
        } elseif {!$quoted && $c in $seps} {
            lappend fields $field
            set field {}
        } else {
            append field $c
        }

        set prev $c
    }

    lappend fields $field

    return $fields
}


namespace eval lib::dict {}

proc lib::dict::getdef args {
    if {[llength $args] < 3} {
        return -code error \
               -errorcode {TCL WRONGARGS} \
               "wrong # args: should be \"[lindex [info level 0] 0]\
                dictionary ?key ...? key default\""
    }

    set dictionary [lindex $args 0]
    set default [lindex $args end]
    set keys [lrange $args 1 end-1]

    set error [catch {
        dict get $dictionary {*}$keys
    } result opts]

    if {!$error} {
        return $result
    }

    if {$error && $result eq {missing value to go with key}} {
        return {*}$opts $result
    }

    return $default
}


# Validate and rename a dictionary's keys.
proc lib::dict::map {mapping srcDict} {
    set destDict {}

    dict for {srcKey dest} $mapping {
        catch {unset default}

        switch [llength $dest] {
            1 { set destKey $dest }
            2 { lassign $dest destKey default }
            default {
                return -code error \
                       -errorcode {JIMLIB DICT MAP BAD-MAPPING} \
                       [list expected {destKey ?default?} but got $dest] }
        }

        if {[info exists srcDict($srcKey)]} {
            set destDict($destKey) $srcDict($srcKey)
        } elseif {[info exists default]} {
            set destDict($destKey) $default
        } else {
            return -code error \
                   -errorcode {JIMLIB DICT MAP MISSING-KEY} \
                   [list missing required key $srcKey]
        }

        dict unset srcDict $srcKey
    }

    if {$srcDict ne {}} {
        return -code error \
               -errorcode {JIMLIB DICT MAP EXTRA-KEYS} \
               [list unknown extra keys: $srcDict]
    }

    return $destDict
}


proc lib::dict::fill-repeats dict {
    if {[llength $dict] % 2 == 1} {
        return -code error \
               -errorcode {JIMLIB DICT FILL-REPEATS NOT-A-DICTIONARY} \
               [list not a dictionary: $dict]
    }

    set prevV {}
    foreach {v k} [lreverse $dict] {
        if {$v eq {-}} {
            dict set dict $k $prevV
        } else {
            set prevV $v
        }
    }

    return $dict
}


namespace eval lib::file {}

proc lib::file::find {{startDir .} args} {
    lib::named-args {
        -filevarname   {fileVarName   file}
        -depthvarname  {depthVarName  depth}
        -filefilter    {fileFilter    {}}
        -filescript    {fileScript    {}}
        -dirvarname    {dirVarName    dir}
        -dirfilter     {dirFilter     {}}
        -followlinks   {followLinks   0}
    }

    set found {}
    set queue [list $startDir 0]

    upvar 1 $dirVarName dir \
            $fileVarName file \
            $depthVarName depth

    while {$queue ne {}} {
        set queue [lassign $queue dir depth]

        if {$dirFilter ne {} && ![uplevel 1 [list expr $dirFilter]]} continue
        if {!$followLinks} {
            file lstat $dir lstat
            if {$lstat(type) eq {link}} continue
        }

        incr depth

        foreach file [list-dir $dir] {
            if {[file isdir $file]} {
                lappend queue $file $depth
                continue
            }

            if {$fileFilter ne {} && ![uplevel 1 [list expr $fileFilter]]} {
                continue
            }

            if {$fileScript eq {}} {
                lappend found $file
            } else {
                set result [uplevel 1 $fileScript]
                # Valid filenames aren't be empty.
                if {$result ne {}} {
                    lappend found $result
                }
            }
        }
    }

    return $found
}


proc lib::file::list-dir args {
    set tails false
    while {[regexp ^- [lindex $args 0]]} {
        set args [lassign $args arg]
        switch -- $arg {
            -- break
            -tails {
                set tails true
            }
            default {
                return -code error \
                       -errorcode {JIMLIB FILE LIST-DIR UNKNOWN-SWITCH} \
                       [list unknown switch $arg]
            }
        }
    }

    if {[llength $args] > 1} {
        return -code error \
               -errorcode {TCL WRONGARGS} \
               "wrong # args: should be \"[lindex [info level 0] 0]\
                ?-switch ...? ?dir?\""
    }
    set dir [lindex $args 0]

    set switches [list -directory $dir -nocomplain]
    set dots [lmap file [glob -tails {*}$switches .*] {
        if {$file in {. ..}} continue

        if {$tails && $dir ne {}} {
            lindex $file
        } else {
            file join $dir $file
        }
    }]

    set normal [glob {*}[expr {$tails ? {-tails} : {}}] {*}$switches *]

    set files [lsort [concat $dots $normal]]

    return $files
}


proc lib::file::read path {
    set ch [open $path rb]
    defer {close $ch}

    return [::read $ch]
}


proc lib::file::reroot {fromDir toDir path} {
    set pathSplit [split $path]
    set fromSplit [split $fromDir]
    set fromLen [llength $fromSplit]

    if {[lrange $pathSplit 0 $fromLen-1] ne $fromSplit} {
        return -code error \
               -errorcode {JIMLIB FILE REROOT BAD-FROMDIR} \
               [list $path not in $fromDir]
    }

    return [file join $toDir {*}[lrange $pathSplit $fromLen end]]
}


namespace eval lib::file::run {
    variable config {script lindex}
}


proc lib::file::run args {
    variable run::config

    if {$args eq {}} return

    {*}$config(script) $args
    exec {*}$args >@ stdout 2>@stderr
}


# This command doesn't understand Windows volumes and paths or UNC.
proc lib::file::split {path {max 1024}} {
    # Detect and strip leading slashes.
    set absolute [regexp ^/+(.*)$ $path _ path]
    set head [expr {!$absolute}]

    set parts {}

    # We can't use [regexp -indices -start N] here because it will cause
    # problems with Unicode in Jim Tcl 0.78 or earlier.
    while {[regexp {^([^/]+)/+(.*)$} $path _ part path]} {
        if {!$head && [regexp ^~ $part]} {
            set part ./$part
        }
        lappend parts $part

        if {[llength $parts] > $max} {
            return -code error \
                   -errorcode {JIMLIB FILE SPLIT TOO-MANY-PARTS} \
                   {aborted: path has too many parts}
        }

        set head 0
    }

    if {$path ne {}} {
        if {!$head && [regexp ^~ $path]} {
            set path ./$path
        }
        lappend parts $path
    }

    if {$absolute} {
        set parts [linsert $parts 0 /]
    }

    return $parts
}


namespace eval lib::file::tar {}

proc lib::file::tar::field {name max value {null true}} {
    upvar 1 formats formats
    upvar 1 values values
    upvar 1 [namespace parent [namespace parent]]::bytelength bytelength

    set null [expr {$null ? 1 : 0}]

    if {$bytelength} {
        set bytes [string bytelength $value]
    } else {
        set bytes [string length $value]
    }

    if {$bytes > $max - $null} {
        return -code error \
               -errorcode {JIMLIB FILE TAR HEADER FIELD-TOO-LONG} \
               [list field $name exceeds [expr {$max - $null}] bytes]
    }

    append formats a$max
    lappend values $value
}


proc lib::file::tar::finish ch {
    puts -nonewline $ch [string repeat \0 1024]
}


proc lib::file::tar::header args {
    lib::named-args {
        -filename filename
        -mode {mode 420}
        -uid {uid 0}
        -gid {gid 0}
        -size size
        -mtime {mtime 0}
        -type {type 0}
        -linkname {linkname {}}
        -uname {uname root}
        -gname {gname root}
        -devmajor {devmajor 0}
        -devminor {devminor 0}
        -prefix {prefix {}}
    }

    set formats {}
    set values {}

    field filename 100 $filename
    field mode     8   [format %07o $mode]
    field uid      8   [format %07o $uid]
    field gid      8   [format %07o $gid]
    field size     12  [format %011o $size]
    field mtime    12  [format %011o $mtime]
    field checksum 8   {        }            false  ;# Checksum.
    field type     1   [format %1o $type]    false
    field linkname 100 $linkname

    field magic    6   ustar
    field version  2   00                    false  ;# Version.
    field uname    32  $uname
    field gname    32  $gname
    field devmajor 8   [format %07o $devmajor]
    field devminor 8   [format %07o $devminor]
    field prefix   155 $prefix  ;# Filename prefix.

    set checksum 0
    foreach value $values {
        binary scan $value cu* bytes
        foreach b $bytes {
            incr checksum $b
        }
    }
    lset values 6 [format %07o $checksum]

    set header [binary format $formats {*}$values]
}


proc lib::file::tar::write {ch data args} {
    upvar 1 [namespace parent [namespace parent]]::bytelength bytelength

    if {$bytelength} {
        set size [string bytelength $data]
    } else {
        set size [string length $data]
    }

    set header [header {*}$args -size $size]
    puts -nonewline $ch $header
    puts -nonewline $ch \0\0\0\0\0\0\0\0\0\0\0\0
    puts -nonewline $ch $data
    if {$size % 512 != 0} {
        puts -nonewline $ch [string repeat \0 [expr {512 - $size % 512}]]
    }
}


proc lib::file::tempdir {} {
    upvar 1 [namespace parent]::jim jim

    if {$jim} {
        set path [file tempfile]
    } else {
        close [file tempfile path]
    }

    file delete $path
    file mkdir $path

    return $path
}


proc lib::file::with-path {path script} {
    set prev [pwd]
    try {
        cd $path
        uplevel 1 $script
    } finally {
        cd $prev
    }
}


proc lib::file::which name {
    set dirs [::split $::env(PATH) :]

    foreach dir $dirs {
        set path [file join $dir $name]
        if {[file exists $path]} {
            return $path
        }
    }

    return {}
}


proc lib::file::write {path contents} {
    set ch [open $path wb]
    defer {close $ch}

    puts -nonewline $ch $contents
}


namespace eval lib::gui {}
namespace eval lib::gui::dialog {
    variable tclsh tclsh
    variable template {
        package require Tk
        wm withdraw .

        set result [%s]
        if {[info exists type]} {
            set result [list $result $type]
        }

        puts $result
        exit 0
    }
}


proc lib::gui::dialog::generic command {
    variable tclsh
    variable template

    set script [format $template $command]

    exec {*}$tclsh << $script
}


proc lib::gui::dialog::directory {initialdir args} {
    generic [list tk_chooseDirectory \
        -initialdir $initialdir \
        {*}$args \
    ]
}


proc lib::gui::dialog::open {initialdir args} {
    generic [list tk_getOpenFile \
        -initialdir $initialdir \
        -typevariable type \
        {*}$args \
    ]
}


proc lib::gui::dialog::save {initialdir args} {
    generic [list tk_getSaveFile \
        -confirmoverwrite true \
        -initialdir $initialdir \
        -typevariable type \
        {*}$args \
    ]
}


namespace eval lib::hash {}

proc lib::hash::mult {s {init 5381} {m 33}} {
    set hash $init
    for {set i 0} {$i < [string bytelength $s]} {incr i} {
        set c [string byterange $s $i $i]
        set hash $($m * $hash + [scan $c %c])
    }
    return $hash
}


proc lib::id value {
    return $value
}


namespace eval lib::html {}

proc lib::html::entities text {
    string map {\" &quot; ' &apos; & &amp; < &lt; > &gt;} $text
}


namespace eval lib::info {}

proc lib::info::showproc args {
    upvar [namespace parent]::jim jim

    set sources [lmap proc $args {
        try {
            set origin [namespace origin $proc]
            set error [expr {
                [info procs $origin] eq {}
            }]
        } on error _ {
            set error 1
        }

        if {$error} {
            return \
                -code error \
                -errorcode {JIMLIB INFO SHOWPROC} \
                [list $proc isn't a procedure] \
        }

        set argsWithDefaults [info args $proc]
        if {!$jim} {
            set argsWithDefaults [lmap arg [info args $proc] {
                if {[info default $proc $arg default]} {
                    list $arg $default
                } else {
                    lindex $arg
                }
            }]
        }

        set statics {}
        if {$jim} {
            set statics [list [lmap {varName value} [info statics $proc] {
                list $varName $value
            }]]
            if {$statics eq {{}}} { set statics {} }
        }

        list proc $proc $argsWithDefaults {*}$statics [info body $proc]
    }]

    if {[llength $args] == 1} {
        set sources [lindex $sources 0]
    }

    return $sources
}


proc lib::inspect {label args} {
    # Prevent name clashes. We let the caller name the variable for [parray].
    if {$label in {__label __procName}} {
        return -code error \
               -errorcode {JIMLIB INSPECT BAD-LABEL} \
               [list invalid label: $label]
    }

    set data {}
    foreach varName $args {
        upvar 1 $varName v
        if {[info exists v]} {
            set data($varName) $v
            continue
        }

        set data($varName) {variable does not exist}
    }

    set __procName ::inspect-$label-puts-stderr
    # Can't use a lambda or a script with [parray].
    local proc $__procName s {
        puts stderr $s
    }

    set __label $label
    set $__label $data

    parray $__label * $__procName
}


namespace eval lib::list {}

proc lib::list::diff {before after} {
    lassign [lcs $before $after] lcs1 lcs2

    set diff {}

    if {$lcs1 eq {}} {
        foreach el $before {
            lappend diff - $el
        }

        foreach el $after {
            lappend diff + $el
        }

        return $diff
    }

    local proc diff-add {symbol source prev current} {
        upvar 1 diff diff

        for {set i $($prev + 1)} {$i < $current} {incr i} {
            lappend diff $symbol [lindex $source $i]
        }
    }

    set prevI -1
    set prevJ -1
    foreach i $lcs1 j $lcs2 {
        diff-add - $before $prevI $i
        diff-add + $after $prevJ $j

        lappend diff = [lindex $before $i]

        set prevI $i
        set prevJ $j
    }
    diff-add - $before $prevI [llength $before]
    diff-add + $after $prevJ [llength $after]

    return $diff
}


namespace eval lib::list::lcs {}

# Return one longest common subsequence of $list1 and $list2.
proc lib::list::lcs {list1 list2} {
    set m [lcs::length $list1 $list2]
    return [lcs::backtrack $list1 $list2 $m [llength $list1] [llength $list2]]
}


proc lib::list::lcs::backtrack {list1 list2 m i j} {
    if {$i == 0 || $j == 0} {
        return {{} {}}
    }

    if {[lindex $list1 $i-1] eq [lindex $list2 $j-1]} {
        lassign [backtrack $list1 $list2 $m $($i - 1) $($j - 1)] bt1 bt2
        return [list [concat $bt1 $($i - 1)] [concat $bt2 $($j - 1)]]
    }

    if {$m($i $($j - 1)) > $m($($i - 1) $j)} {
        tailcall backtrack $list1 $list2 $m $i $($j - 1)
    }

    tailcall backtrack $list1 $list2 $m $($i - 1) $j
}


proc lib::list::lcs::length {list1 list2} {
    set len1 [llength $list1]
    set len2 [llength $list2]

    for {set i 0} {$i <= $len1} {incr i} {
        set "m($i 0)" 0
    }
    for {set j 0} {$j <= $len2} {incr j} {
        set "m(0 $j)" 0
    }

    for {set i 1} {$i <= $len1} {incr i} {
        for {set j 1} {$j <= $len2} {incr j} {
            if {[lindex $list1 $i-1] eq [lindex $list2 $j-1]} {
                set "m($i $j)" $(1 + $m($($i - 1) $($j - 1)))
            } else {
                set "m($i $j)" [lib::math::max $m($($i - 1) $j) \
                                               $m($i $($j - 1))]
            }
        }
    }

    return $m
}


proc lib::list::mapv args {
    set body [lindex $args end]
    set args [lrange $args 0 end-1]

    set command {puts stderr}
    if {[lindex $args 0] eq {-command}} {
        set args [lassign $args _ command]
    }
    set varLists {}
    set steps {}
    set lens {}

    set n 0
    foreach {varList list} $args {
        lappend varLists $varList
        lappend steps [llength $varList]
        lappend lens [llength $list]
        incr n
    }

    set procName ::process-[expr rand()]
    set counterVarName $procName-counter
    proc $procName {command varLists steps lens n counterVarName} {
        set msg {}

        foreach varList $varLists step $steps len $lens {
            set pos [expr { [set $counterVarName] * $step }]

            lappend msg $varList [expr {
                $pos < $len ? "$pos<$len" : {done}
            }]
        }

        uplevel 1 [list {*}$command $msg]
        incr $counterVarName

        return
    }

    set procCmd [list \
        $procName \
        $command \
        $varLists \
        $steps \
        $lens \
        $n \
        $counterVarName \
    ]
    set $counterVarName 0
    set result [uplevel 1 [list lmap {*}$args $procCmd\n$body]]

    uplevel 1 $procCmd
    rename $procName {}
    unset $counterVarName

    return $result
}


proc lib::list::pick {list indices} {
    set res {}

    foreach i $indices {
        lappend res [lindex $list $i]
    }

    return $res
}


proc lib::list::product args {
    set n [llength $args]
    if {$n == 0} { return {} }

    set top "    set product {}\n"
    set middle {        lappend product [list}
    set bottom {}

    for {set i 0} {$i < $n} {incr i} {
        append top [format {    foreach elem(%1$u) [lindex $lists %1$u] } $i]
        append top \{\n

        append middle " \$elem($i)"

        append bottom "\n    \}"
    }
    append middle \]

    set lambda "lists {\n$top$middle$bottom\n    return \$product\n}"

    set product [apply $lambda $args]

    return $product
}


proc lib::list::sl {script {prefix list}} {
    set list [lmap command [lib::commands $script] {
        if {[string index $command 0] eq {#}} continue
        uplevel 1 {*}$prefix $command
    }]

    return [concat {*}$list]
}


proc lib::list::transpose lol {
    set n [llength $lol]

    set lens [lmap x $lol {llength $x}]
    set len [lindex [lsort -integer $lens] 0]

    set tr {}
    for {set i 0} {$i < $len} {incr i} {
        set xs {}

        for {set j 0} {$j < $n} {incr j} {
            lappend xs [lindex $lol $j $i]
        }

        lappend tr $xs
    }

    return $tr
}


proc lib::list::zip args {
    tailcall transpose $args
}


namespace eval lib::log {
    variable config {
        channel stderr
        format {== $timestamp ($level) [list $message]}
        gmt 1
        level info
        levels {emerg alert crit err warning notice info debug discard}
        timestamp {%Y-%m-%d %H:%M:%S UTC}
    }
}


proc lib::log {level message} {
    variable log::config

    set levelNumber {level {
        upvar 1 config config

        set i [lsearch [dict get $config levels] $level]
        if {$i == -1} {
            return -code error \
                   -errorcode {JIMLIB LOG BAD-LEVEL} \
                   [list unknown log level $level]
        }

        return $i
    }}

    if {[apply $levelNumber [dict get $config level]]
        < [apply $levelNumber $level]} {
        return
    }

    set timestamp [clock format \
        [clock seconds] \
        -format [dict get $config timestamp] \
        -gmt [dict get $config gmt] \
    ]

    puts [dict get $config channel] [subst [dict get $config format]]
}


namespace eval lib::math {
    variable pi 3.14159265358979323846
    variable halfPi [expr {$pi / 2}]
}


proc lib::math::atan2 {y x} {
    variable pi
    variable halfPi

    if {$x > 0} {
        return $(atan($y / $x))
    } elseif {$x < 0 && $y >= 0} {
        return $(atan($y / $x) + $pi)
    } elseif {$x < 0 && $y < 0} {
        return $(atan($y / $x) - $pi)
    } elseif {$x == 0 && $y > 0} {
        return $halfPi
    } elseif {$x == 0 && $y < 0} {
        return -$halfPi
    } else {
        # By convention.
        return 0.0
    }
}


namespace eval lib::math::base-n {}

proc lib::math::base-n::from-decimal {chars a} {
    set n [llength $chars]

    set b {}
    while {$a > 0} {
        set digit [expr { $a % $n }]
        set b [lindex $chars $digit]$b
        set a [expr { ($a - $digit) / $n }]
    }

    return $b
}


proc lib::math::base-n::to-decimal {chars a} {
    set n [llength $chars]

    set values [dict create]
    set i 0
    foreach char $chars {
        dict set values $char $i
        incr i
    }

    set dec 0
    foreach char [split $a {}] {
        set dec [expr { $dec * $n }]
        incr dec [dict get $values $char]
    }

    return $dec
}


namespace eval lib::math::complex {}

proc lib::math::complex::+ {z1 z2} {
    lassign $z1 a1 b1
    lassign $z2 a2 b2
    if {$b1 eq {}} { set b1 0 }
    if {$b2 eq {}} { set b2 0 }
    return [list $($a1 + $a2) $($b1 + $b2)]
}


proc lib::math::complex::* {z1 z2} {
    lassign $z1 a1 b1
    lassign $z2 a2 b2
    if {$b1 eq {}} { set b1 0 }
    if {$b2 eq {}} { set b2 0 }
    return [list $($a1*$a2 - $b1*$b2) $($a1*$b2 + $a2*$b1)]
}


proc lib::math::complex::mod z {
    lassign $z a b
    if {$b eq {}} { set b 0 }
    return $(sqrt($a*$a + $b*$b))
}


proc lib::math::max args {
    return [lindex [lsort -real $args] end]
}


proc lib::math::min args {
    return [lindex [lsort -real $args] 0]
}


namespace eval lib::mp {}

proc lib::mp::pmap {varList list body chunkSize} {
    set tasks {}
    for {set start 0} {$start < [llength $list]} {incr start $chunkSize} {
        set end $($start + $chunkSize - 1)
        set chunk [lrange $list $start $end]
        set cmd [list \
            | [info nameofexecutable] \
              -e "lmap [list $varList] \[gets stdin\] [list $body]" \
              << $chunk \
              2>@1 \
        ]
        lappend tasks [open $cmd] $start $end
    }

    set results [lmap {ch start end} $tasks {
        set output [gets $ch]

        set pid [pid $ch]
        lassign [wait $pid] _ _ status
        if {$status != 0} {
            return -code error \
                   -errorcode [list JIMLIB MP PMAP CHILDSTATUS $status] \
                   [list child process $pid for chunk $start to $end exited \
                         with status $status and message $output]
        }

        lindex $output
    }]

    return [concat {*}$results]
}


namespace eval lib::named-args {}

proc lib::named-args {spec {positional 0} {help 1}} {
    upvar 1 args args

    if {$help
        && [llength $args] == 1
        && [string match -*help $args]} {
        return -level 2 $spec
    }

    lassign [lib::named-args::parse $spec $positional $args] vars args
    foreach {k v} $vars {
        uplevel 1 [list set $k $v]
    }

    return $args
}


proc lib::named-args::parse {spec positional arguments} {
    set spec [lib::dict::fill-repeats $spec]

    set missingVars {}
    set parsed {}
    foreach {flag v} $spec {
        lassign $v varName default options

        if {[llength $v] > 1} {
            dict set parsed $varName $default
        }

        if {[llength $v] == 1 || {required} in $options} {
            dict lappend missingVars $varName $flag
        }
    }

    set unknownArgs {}

    while {$arguments ne {}} {
        set arg [lindex $arguments 0]
        if {$arg eq {--} || ![string match -* $arg]} break
        set arguments [lrange $arguments 1 end]

        if {[dict exists $spec $arg]} {
            catch {unset varName default}
            set options {}

            lassign [dict get $spec $arg] varName default options

            dict unset missingVars $varName

            if {{flag} in $options} {
                continue
            }

            set arguments [lassign $arguments value]
            dict set parsed $varName $value

            continue
        }

        lappend unknownArgs $arg
    }

    if {$missingVars ne {}} {
        return -code error \
               -errorcode {JIMLIB NAMED-ARGS PARSE MISSING} \
               [list missing required argument {*}[dict values $missingVars]]
    }

    if {!$positional} {
        lappend unknownArgs {*}$arguments
    }

    if {$unknownArgs ne {}} {
        return -code error \
               -errorcode {JIMLIB NAMED-ARGS PARSE EXTRA} \
               [list unknown extra arguments: $unknownArgs]
    }

    return [list $parsed $arguments]
}


namespace eval lib::namespace {}

proc lib::namespace::ensemble namespace {
    proc $namespace args namespace {
        set args [lassign $args command]

        set nsCommand [set namespace]::$command

        if {[info commands $nsCommand] eq {}} {
            set available [lsort [lmap x [info commands [set namespace]::*] {
                namespace tail $x
            }]]

            set mustBe $available
            if {[llength $available] > 1} {
                set mustBe [join [lrange $available 0 end-1] {, }]
                append mustBe ", or [lindex $available end]"
            }

            return \
                -code error \
                -errorcode [list TCL LOOKUP SUBCOMMAND $command] \
                "unknown or ambiguous subcommand \"$command\":\
                 must be $mustBe"
        }

        tailcall $nsCommand {*}$args
    }
}


namespace eval lib::net {}

proc lib::net::curl args {
    try {
        exec curl \
            --fail \
            --location \
            --show-error \
            --silent \
            {*}$args \
            2>@1
    } on error {result opts} {
        set errorcode {JIMLIB NET CURL}
        if {[regexp {returned error: ([0-9]+)} $result _ httpError]} {
            lappend errorcode HTTP $httpError
        }
        return -code error \
               -errorcode $errorcode \
               [lindex [split $result \n] 0]
    }
}


namespace eval lib::oo {}

proc lib::oo::class {name body {debug false}} {
    namespace eval ::oo {}

    set qualified [uplevel 1 [list namespace eval $name {
        namespace current
    }]]

    namespace delete $qualified
    namespace eval $qualified {
        variable counter 0

        variable constructor {}
        variable methods {
            {destroy {} { namespace delete [namespace current] }}
        }
        variable variables {}

        proc constructor args {
            set [namespace current]::constructor $args
        }

        proc method args {
            lappend [namespace current]::methods $args
        }

        proc variable name {
            lappend [namespace current]::variables $name
        }
    }

    namespace eval $qualified $body

    lassign [set ${qualified}::constructor] consArguments consBody

    set mapping [list \
        %CONS_ARGUMENTS% [list $consArguments] \
        %CONS_BODY% [list $consBody] \
        %NS% [list ::oo$qualified] \
        %VARIABLES% [list [join [lmap x [set ${qualified}::variables] {
            lindex "variable [list $x]"
        }] \n]] \
        %METHODS% [join [lmap x [set ${qualified}::methods] {
            lindex "method $x"
        }] \n] \
    ]

    set new [list proc ${qualified}::new args [string map $mapping {
        ::variable counter

        set ns %NS%
        append ns ::handle$counter
        incr counter

        lib::namespace::ensemble $ns

        local proc method {name arguments body} ns {
            set variables %VARIABLES%
            proc [set ns]::$name $arguments $variables\n$body
        }

        %METHODS%

        try {
            method constructor %CONS_ARGUMENTS% %CONS_BODY%
            namespace eval $ns [list constructor {*}$args]
        } on error e {
            namespace delete $ns
            return -code error $e
        }

        return $ns
    }]]

    if {$debug} {
        puts stderr "CREATING CLASS $name\n<<<\n$new\n>>>"
    }

    {*}$new

    return $qualified
}


proc lib::pipe {varName args} {
    upvar 1 $varName v
    set v {}

    set first 1
    while {$args ne {}} {
        set args [lassign $args arg]
        set mode {}

        switch -- $arg {
            -all -
            -apply {
                set mode APPLY
            }

            -each {
                set mode EACH
            }

            -eachline -
            -eachLine {
                set mode EACH_SEP
                set sep \n
            }

            -eachsep -
            -eachSep {
                set mode EACH_SEP
                set args [lassign $args sep]
            }

            -filter {
                set mode FILTER
            }

            -flatmap -
            -flatMap -
            -map* {
                set mode FLAT_MAP
            }

            -flatten {
                set mode FLATTEN
            }

            -init -
            -initial -
            -value {
                if {!$first && [string index $arg 1] eq {i}} {
                    return \
                        -code error \
                        -errorcode {JIMLIB PIPE INITIAL} \
                        {-initial used not as first argument} \
                }

                set mode VALUE
            }

            -fold -
            -reduce {
                set mode REDUCE

                set args [lassign $args accVarName accInitial]
                upvar 1 $accVarName acc
                set acc $accInitial
            }

            -map {
                set mode MAP
            }

            -mapexpr -
            -mapExpr -
            -map$ {
                set mode MAP_EXPR
            }

            -maplines -
            -mapLines {
                set mode MAP_SEP
                set sep \n
            }

            -mapsep -
            -mapSep {
                set mode MAP_SEP
                set args [lassign $args sep]
            }

            -partition {
                set mode PARTITION
            }

            -sort {
                set mode SORT
            }
        }

        if {$mode ne {}} {
            if {[llength $args] == 0} {
                return \
                    -code error \
                    -errorcode {JIMLIB PIPE MISSING} \
                    [list too few arguments for option $arg]
            }

            set args [lassign $args arg]
        }

        switch $mode {
            EACH -
            EACH_SEP -
            FLAT_MAP -
            MAP -
            MAP_EXPR -
            MAP_SEP {
                switch $mode {
                    EACH_SEP -
                    MAP_SEP {
                        set v [split $v $sep]
                    }

                    MAP_EXPR {
                        set arg [list expr $arg]
                    }
                }

                set command [expr {
                    $mode in {EACH EACH_SEP} ? {foreach} : {lmap}
                }]

                set v [uplevel 1 [list $command $varName $v $arg]]

                switch $mode {
                    FLAT_MAP {
                        set v [concat {*}$v]
                    }
                }
            }

            FILTER {
                set script "
                    if {!\[[list expr $arg]\]} continue
                    lindex $[list $varName]
                "
                set v [uplevel 1 [list lmap $varName $v $script]]
            }

            FLATTEN {
                for {set i 0} {$i < $arg} {incr i} {
                    set v [concat {*}$v]
                }
            }

            REDUCE {
                set items $v
                foreach v $items {
                    set acc [uplevel 1 $arg]
                }

                set v $acc
            }

            PARTITION {
                if {$arg < 1} {
                    error [list can't partition by $arg < 1]
                }

                set len [llength $v]
                set partitioned {}
                for {set i 0} {$i < $len} {incr i $arg} {
                    lappend partitioned [lrange $v $i [expr { $i + $arg - 1 }]]
                }

                set v $partitioned
            }

            SORT {
                set items $v
                set scores [uplevel 1 [list lmap $varName $v $arg]]
                set sorted [lsort \
                    -index 0 \
                    -real \
                    [lmap score $scores item $items {
                        list $score $item
                    }] \
                ]
                unset scores

                set v [lmap tuple $sorted { lindex $tuple 1 }]
            }

            VALUE {
                set v $arg
            }

            default {
                set v [uplevel 1 $arg]
            }
        }

        set first 0
    }

    return $v
}


lib::oo::class lib::queue {
    variable _redis
    variable _key

    constructor {redis key} {
        set _redis $redis
        set _key $key
    }

    method add args {
        if {[llength $args] == 0} {
            return [length]
        }

        $_redis rpush $_key {*}$args
    }

    method empty? {} {
        expr { [$_redis llen $_key] == 0 }
    }

    method head {} {
        if {[$_redis llen $_key] == 0} {
            return \
                -code error \
                -errorcode {JIMLIB QUEUE EMPTY} \
                {empty queue}
        }

        $_redis lindex $_key 0
    }

    method length {} {
        $_redis llen $_key
    }

    method list {} {
        $_redis lrange $_key 0 -1
    }

    method reset {} {
        $_redis del $_key

        return {}
    }

    method get {{n 1}} {
        if {$n <= 0} return

        set len [$_redis llen $_key]
        if {$len == 0} {
            return \
                -code error \
                -errorcode {JIMLIB QUEUE EMPTY} \
                {empty queue}
        }

        if {$n == 1} {
            return [$_redis lpop $_key]
        }

        if {$len < $n} {
            return \
                -code error \
                -errorcode {JIMLIB QUEUE NOT-ENOUGH} \
                {empty queue}
        }

        # Supported in Redis 6.2 and later.
        $_redis lpop $_key $n
    }
}


proc lib::queue::worker {queue varName body {sleepInterval 100}} {
    upvar 1 $varName v

    while true {
        while {![$queue empty?]} {
            set v [$queue get]
            try {
                uplevel 1 $body
            } on break result {
                return $result
            }
        }

        after $sleepInterval
    }
}


namespace eval lib::redis {}

proc lib::redis::new {{server localhost:6379}} {
    return [redis [socket stream $server]]
}


namespace eval lib::schema {}
namespace eval lib::schema::validate {}

proc lib::schema::validate {check value} {
    if {![regexp {\s} $check]} {
        return [string is $check -strict $value]
    }

    return [uplevel 1 [list {*}$check $value]]
}


proc lib::schema::validate::dict {schema dict} {
    set failed {}

    foreach {key check default} $schema {
        set good true

        if {[::dict exists $dict $key]} {
            set command [list \
                [namespace parent]::validate \
                $check \
                [::dict get $dict $key] \
            ]

            set good [uplevel 1 $command]
        } else {
            set good false
        }

        if {!$good} {
            lappend failed $key
            ::dict set dict $key $default
        }
    }

    return [list $dict $failed]
}


namespace eval lib::set {}

proc lib::set::diff {a b} {
    set diff {}

    foreach x $a {
        if {$x ni $b} {
            lappend diff $x
        }
    }

    return $diff
}


proc lib::set::subset? {a b} {
    foreach x $a {
        if {$x ni $b} {
            return 0
        }
    }

    return 1
}


proc lib::set::sym-diff {a b} {
    set inters {}
    set symDiff {}

    foreach x $a {
        if {$x in $b} {
            lappend inters $x
        } else {
            lappend symDiff $x
        }
    }

    foreach x $b {
        if {$x ni $inters} {
            lappend symDiff $x
        }
    }

    return [list $symDiff $inters]
}


namespace eval lib::string {}

# A slow Unicode-agnostic [string first].
proc lib::string::bytefirst {needle haystack} {
    set bytesNeedle [string bytelength $needle]
    set bytesHaystack [string bytelength $haystack]

    set n $($bytesHaystack - $bytesNeedle)
    for {set i 0} {$i <= $n} {incr i} {
        set range [string byterange $haystack $i $($i + $bytesNeedle - 1)]
        if {$range eq $needle} {
            return $i
        }
    }

    return -1
}


proc lib::string::indent {string prefix} {
    set lines [lmap line [split $string \n] { lindex $prefix$line }]
    return [join $lines \n]
}


proc lib::string::match-bracket args {
    if {[llength $args] < 1} {
        return -code error \
               -errorcode {TCL WRONGARGS} \
               "wrong # args: should be \"[info level 0] ?-switch ...? s\""
    }

    set s [lindex $args end]
    set args [lrange $args 0 end-1]

    lib::named-args {
        -opening {opening (}
        -closing {closing )}
        -escape {escape \\}
        -start {start 0}
        -n {n 0}
    }

    if {[string index $s $start] ne $opening} {
        return -code error \
               -errorcode {JIMLIB STRING MATCH-BRACKET BAD-START} \
               {start isn't a bracket}
    }

    for {set i $start} {$i < [string length $s]} {incr i} {
        set c [string index $s $i]

        if {$c eq $escape} {
            incr i
            continue
        }

        if {$c eq $opening} {
            incr n
        } elseif {$c eq $closing} {
            incr n -1
        }

        if {$n == 0} break
    }

    if {$n > 0} {
        return -code error \
               -errorcode {JIMLIB STRING MATCH-BRACKET UNMATCHED} \
               [list unmatched brackets: $n]
    }

    return $i
}


proc lib::string::pandoc-id s {
    set s [string tolower $s]
    regsub -all {[^a-z0-9 ._-]} $s {} s
    regsub -all { +} $s - s
    regsub -all {^[^a-z]*} $s {} s

    if {$s eq ""} { return section }
    return $s
}


# Split $str on separators that match $regexp. Returns a list consisting of
# fields and, if $includeSeparators is 1, the separators after each.
proc lib::string::sepsplit {str regexp {includeSeparators 1}} {
    if {$str eq {}} {
        return {}
    }
    if {$regexp eq {}} {
        return [split $str {}]
    }
    # Thanks to KBK for the idea.
    if {[regexp $regexp {}]} {
        return -code error \
               -errorcode {JIMLIB STRING SEPSPLIT INFINITE-LOOP}
               [list splitting on regexp $regexp would cause infinite loop]
    }

    # Split $str into a list of fields and separators.
    set fieldsAndSeps {}
    set offset 0
    while {[regexp -start $offset -indices -- $regexp $str match]} {
        lassign $match matchStart matchEnd
        lappend fieldsAndSeps \
                [string range $str $offset [expr {$matchStart - 1}]]
        if {$includeSeparators} {
            lappend fieldsAndSeps \
                    [string range $str $matchStart $matchEnd]
        }
        set offset [expr {$matchEnd + 1}]
    }
    # Handle the remainder of $str after all the separators.
    set tail [string range $str $offset end]
    if {$tail eq {}} {
        # $str ended on a separator.
        if {!$includeSeparators} {
            lappend fieldsAndSeps {}
        }
    } else {
        lappend fieldsAndSeps $tail
        if {$includeSeparators} {
            lappend fieldsAndSeps {}
        }
    }

    return $fieldsAndSeps
}


proc lib::string::slugify text {
    string trim [regsub -all {[^[:alnum:]]+} [string tolower $text] -] -
}


proc lib::string::unindent {text args} {
    lib::named-args {
        -chars {chars { }}
        -ignoreindentonly {ignoreIndentOnlyLines true}
        -max {max inf}
    }

    regsub ^\n $text {} text
    if {$ignoreIndentOnlyLines} {
        regsub \n\[$chars\]*?$ $text {} text
    } else {
        regsub \n$ $text {} text
    }

    set rLeading ^\[$chars\]*
    set rBlankLine $rLeading$

    foreach line [split $text \n] {
        if {$line eq {}
            || ($ignoreIndentOnlyLines
                && [regexp $rBlankLine $line])} continue

        regexp -indices $rLeading $line idc
        set count [expr {[lindex $idc 1] + 1}]

        set max [expr {$max > $count ? $count : $max}]
    }

    set start [expr { $max == inf ? {end+1} : $max }]

    join [lmap line [split $text \n] {
        string range $line $start end
    }] \n
}


namespace eval lib::template {}

proc lib::template::eval {template {vars {}}} {
    set code {}

    dict for {varName value} $vars {
        append code [list set $varName $value]\n
    }
    append code [parse $template]

    return [uplevel 1 [list apply [list {} $code]]]
}


# Convert a template into Tcl code.
proc lib::template::parse template {
    set result {}
    set regExpr {^(.*?)<%(.*?)%>(.*)$}
    set listing "set _output {}\n"
    while {[regexp $regExpr $template match preceding token template]} {
        append listing [list append _output $preceding]\n

        switch -exact -- [string index $token 0] {
            & {
                set code [list [string range $token 1 end]]
                append listing [format \
                    {append _output [%s [expr %s]]} \
                    lib::html::entities \
                    $code \
                ]
            }
            = {
                set code [list [string range $token 1 end]]
                append listing [format {append _output [expr %s]} $code]
            }
            ! {
                set code [string range $token 1 end]
                append listing [format {append _output [%s]} $code]
            }
            default {
                append listing $token
            }
        }
        append listing \n
    }

    append listing [list append _output $template]\n

    return $listing
}


namespace eval lib::term {}
namespace eval lib::term::ansi {}

proc lib::term::ansi::attrs args {
    set attrs {}
    foreach call $args {
        set cmd {}
        for {set i 0} {$i < [llength $call]} {incr i} {
            if {[string is integer [lindex $call $i]]} break
            lappend cmd [lindex $call $i]
        }

        lappend attrs {*}[[join $cmd ::] {*}[lrange $call $i end]]
    }

    return [format $attrs]
}

proc lib::term::ansi::format attrs {
    return \033\[[join $attrs \;]m
}

proc lib::term::ansi::rgb-to-8-bit {r g b} {
    tailcall rgb5-to-8-bit \
             [expr {$r * 6 / 256}] \
             [expr {$g * 6 / 256}] \
             [expr {$b * 6 / 256}]
}

proc lib::term::ansi::rgb5-to-8-bit {r g b} {
    if {!(   0 <= $r && $r <= 5
          && 0 <= $g && $g <= 5
          && 0 <= $b && $b <= 5)} {
        return -code error \
               -errorcode {JIMLIB TERM ANSI RGB5-TO-8-BIT OUT-OF-RANGE} \
               [list rgb values [list $r $g $b] not in range of 0 to 5]
    }
    return [expr {16 + 36*$r + 6*$g + $b}]
}

proc lib::term::ansi::strip text {
    regsub -all {\x1b\[[0-9;]*m} $text {}
}

namespace eval lib::term::ansi::bold {}
proc lib::term::ansi::bold {} { return 1 }
proc lib::term::ansi::bold::off {} { return 22 }
namespace eval lib::term::ansi::faint {}
proc lib::term::ansi::faint {} { return 2 }
proc lib::term::ansi::faint::off {} { return 22 }
namespace eval lib::term::ansi::italic {}
proc lib::term::ansi::italic {} { return 3 }
proc lib::term::ansi::italic::off {} { return 23 }
namespace eval lib::term::ansi::underline {}
proc lib::term::ansi::underline {} { return 4 }
proc lib::term::ansi::underline::double {} { return 21 }
proc lib::term::ansi::underline::off {} { return 24 }
namespace eval lib::term::ansi::blink {}
proc lib::term::ansi::blink {} { return 5 }
proc lib::term::ansi::blink::fast {} { return 6 }
proc lib::term::ansi::blink::off {} { return 25 }
namespace eval lib::term::ansi::reverse {}
proc lib::term::ansi::reverse {} { return 7 }
proc lib::term::ansi::reverse::off {} { return 27 }
namespace eval lib::term::ansi::conceal {}
proc lib::term::ansi::conceal {} { return 8 }
proc lib::term::ansi::conceal::off {} { return 28 }
namespace eval lib::term::ansi::strikethrough {}
proc lib::term::ansi::strikethrough {} { return 9 }
proc lib::term::ansi::strikethrough::off {} { return 29 }
namespace eval lib::term::ansi::overline {}
proc lib::term::ansi::overline {} { return 53 }
proc lib::term::ansi::overline::off {} { return 55 }

proc lib::term::ansi::custom args { return $args }

namespace eval lib::term::ansi::fg {}
proc lib::term::ansi::fg::black {} { return 30 }
proc lib::term::ansi::fg::red {} { return 31 }
proc lib::term::ansi::fg::green {} { return 32 }
proc lib::term::ansi::fg::yellow {} { return 33 }
proc lib::term::ansi::fg::blue {} { return 34 }
proc lib::term::ansi::fg::magenta {} { return 35 }
proc lib::term::ansi::fg::cyan {} { return 36 }
proc lib::term::ansi::fg::white {} { return 37 }
proc lib::term::ansi::fg::8-bit color { list 38 5 $color }
proc lib::term::ansi::fg::rgb {r g b} { list 38 2 $r $g $b }
proc lib::term::ansi::fg::default {} { return 39 }

namespace eval lib::term::ansi::bg {}
proc lib::term::ansi::bg::black {} { return 40 }
proc lib::term::ansi::bg::red {} { return 41 }
proc lib::term::ansi::bg::green {} { return 42 }
proc lib::term::ansi::bg::yellow {} { return 43 }
proc lib::term::ansi::bg::blue {} { return 44 }
proc lib::term::ansi::bg::magenta {} { return 45 }
proc lib::term::ansi::bg::cyan {} { return 46 }
proc lib::term::ansi::bg::white {} { return 47 }
proc lib::term::ansi::bg::8-bit color { list 48 5 $color }
proc lib::term::ansi::bg::rgb {r g b} { list 48 2 $r $g $b }
proc lib::term::ansi::bg::default {} { return 49 }


proc lib::test args {
    if {[llength $args] < 4} {
        return -code error \
               -errorcode {TCL WRONGARGS} \
               "wrong # args: should be
                \"[info level 0] name ?-switch ...? script arrow expected\""
    }

    set name [lindex $args 0]
    lassign [lrange $args end-2 end] script arrow expected
    set args [lrange $args 1 end-3]

    named-args {
        -constraints {myConstraints {}}
    }

    upvar stats stats
    upvar constraints constraints

    dict incr stats total
    foreach constr $myConstraints {
        set positive [expr {
            [string index $constr 0] ne {!}
        }]
        if {
            ($positive && $constr ni $constraints)
            || (!$positive && [string range $constr 1 end] in $constraints)
        } {
            dict incr stats skipped
            return
        }
    }

    catch $script result

    set matched [switch -- $arrow {
        ->   { expr {$result eq $expected} }
        ->*  { string match $expected $result }
        ->$  { regexp -- $expected $result }
        default {
            return -code error \
                   -errorcode {JIMLIB TEST BAD-ARROW} \
                   [list unknown arrow: $arrow]
        }
    }]

    if {!$matched} {
        set error {}
        append error "\n>>>>> $name failed: [list $script]\n"
        append error "      got: [list $result]\n"
        append error " expected: [list $expected]"
        if {$arrow ne {->}} {
            append error "\n    match: $arrow"
        }

        puts stderr $error

        dict incr stats failed
        return
    }

    dict incr stats passed
}


namespace eval ::tcl {}
namespace eval ::tcl::control {}

proc lib::try args {
    # These are not local, since this allows us to [uplevel] a [catch] rather
    # than [catch] the [uplevel]ing of something, resulting in a cleaner
    # -errorinfo:
    set EMVAR  ::tcl::control::em
    set OPTVAR ::tcl::control::opts
    upvar $EMVAR em
    upvar $OPTVAR opts

    set magicCodes { ok 0 error 1 return 2 break 3 continue 4 signal 5 }

    # ----- Parse arguments -----

    set trybody [lindex $args 0]
    set finallybody {}
    set handlers [list]
    set i 1

    while {$i < [llength $args]} {
        switch -- [lindex $args $i] {
            "on" {
                incr i
                set code [lindex $args $i]

                if {[dict exists $magicCodes $code]} {
                    set code [dict get $magicCodes $code]
                } elseif {![string is integer -strict $code]} {
                    set msgPart [join [dict keys $magicCodes] {", "}]
                    return -code error \
                           -errorcode {JIMLIB TRY BAD-CODE} \
                           "bad code '[lindex $args $i]': must be\
                            integer or \"$msgPart\""
                }

                lappend handlers \
                    [lrange $args $i $i] \
                    [format %d $code] \
                    {} \
                    {*}[lrange $args $i+1 $i+2]

                incr i 3
            }

            "trap" {
                incr i

                if {[catch {llength [lindex $args $i]}]} {
                    return -code error \
                           -errorcode {JIMLIB TRY BAD-PREFIX} \
                           "bad prefix '[lindex $args $i]':\
                            must be a list"
                }

                lappend handlers \
                    [lrange $args $i $i] \
                    1 \
                    {*}[lrange $args $i $i+2]

                incr i 3
            }

            "finally" {
                incr i
                set finallybody [lindex $args $i]

                incr i
                break
            }

            default {
                return -code error \
                       -errorcode {JIMLIB TRY BAD-HANDLER} \
                       "bad handler '[lindex $args $i]': must be\
                        \"on code varlist body\", or\
                        \"trap prefix varlist body\""
            }
        }
    }

    if {($i != [llength $args]) || ([lindex $handlers end] eq "-")} {
        return -code error \
               -errorcode {TCL WRONGARGS} \
               "wrong # args: should be\
                \"try body ?handler ...? ?finally body?\""
    }

    # ----- Execute 'try' body -----

    set code [uplevel 1 [list ::catch $trybody $EMVAR $OPTVAR]]

    # Keep track of the original error message & options
    set _em $em
    set _opts $opts
    # ----- Find and execute handler -----

    set errorcode {}
    if {[dict exists $opts -errorcode]} {
        set errorcode [dict get $opts -errorcode]
    }
    set found 0

    foreach {descrip oncode pattern varlist body} $handlers {
        if {!$found} {
            if {
                ($code != $oncode)
                || ([lrange $pattern 0 end] ne
                    [lrange $errorcode 0 [llength $pattern]-1])
            } {
                continue
            }
        }
        set found 1
        if {$body eq "-"} {
            continue
        }

        # Handler found ...

        # Assign trybody results into variables
        lassign $varlist resultsVarName optionsVarName
        if {[llength $varlist] >= 1} {
            upvar 1 $resultsVarName resultsvar
            set resultsvar $em
        }
        if {[llength $varlist] >= 2} {
            upvar 1 $optionsVarName optsvar
            set optsvar $opts
        }

        # Execute the handler
        set code [uplevel 1 [list ::catch $body $EMVAR $OPTVAR]]

        # Handler result replaces the original result (whether success or
        # failure); capture context of original exception for reference.
        set _em $em
        set _opts $opts

        # Handler has been executed - stop looking for more
        break
    }

    # No catch handler found -- error falls through to caller
    # OR catch handler executed -- result falls through to caller

    # ----- If we have a finally block then execute it -----

    if {$finallybody ne {}} {
        set code [uplevel 1 [list ::catch $finallybody $EMVAR $OPTVAR]]

        # Finally result takes precedence except on success

        if {$code != 0} {
            set _em $em
            set _opts $opts
        }

        # Otherwise our result is not affected
    }

    # Propagate the error or the result of the executed catch body to the
    # caller.
    dict incr _opts -level
    return -code [dict get $_opts -code] \
           -level [dict get $_opts -level] \
           $_em
}


namespace eval lib::ulid {
    variable defaultPRNG prng
    variable base32 [split 0123456789ABCDEFGHJKMNPQRSTVWXYZ {}]
}


# Generate a ULID. See https://wiki.tcl-lang.org/page/ULID.
proc lib::ulid {{t {}} {prng {}}} {
    variable ulid::defaultPRNG

    if {$t eq {}} {
        set t [clock milliseconds]
    }

    if {$prng eq {}} {
        set prng $defaultPRNG
    }

    return [ulid::encode-time $t 10][ulid::gen-random $prng 16]
}


proc lib::ulid::encode-time {t len} {
    variable base32

    if {$t < 0 || $t > 0xffffffffffff} {
        return \
            -code error \
            -errorcode {JIMLIB ULID ENCODE-TIME}
            [list expected unsigned integer representable in 48 bits\
                  but got $t]
    }

    set result {}
    for {set i 0} {$i < $len} {incr i} {
        set m [expr {$t % 32}]
        set result [lindex $base32 $m]$result
        set t [expr {($t - $m) / 32}]
    }

    return $result
}


proc lib::ulid::gen-random {prng len} {
    variable base32

    set result {}
    for {set i 0} {$i < $len} {incr i} {
        append result [lindex $base32 [{*}$prng]]
    }

    return $result
}


proc lib::ulid::prng {} {
    return [expr {int(32 * rand())}]
}


proc lib::run-tests {} {
    variable jim

    set stats { total 0 passed 0 skipped 0 failed 0 }
    set constraints {}

    if {$jim} {
        lappend constraints jim

        try {
            package require redis

            redis::new
        } on ok r {
            if {[$r ping] eq {PONG}} {
                lappend constraints redis
            }
        } on error _ {}
    }

    if {$::tcl_platform(pointerSize) == 8} {
        lappend constraints 64-bit
    }

    foreach cmd {gzip tar no-such-thing} {
        if {[lib::file::which $cmd] ne {}} {
                lappend constraints $cmd
        }
    }

    try {
        if {[exec uname] eq {OpenBSD}} {
            lappend constraints openbsd
        }
    } on error _ {}

    try {
        expr !false
        lappend constraints logical-negation-accepts-text
    } on error _ {}

    test file::split.1.1 {file::split /foo/bar/baz}     -> {/ foo bar baz}
    test file::split.1.2 {file::split /foo/bar/baz/}    -> {/ foo bar baz}
    test file::split.1.3 {file::split ///foo/bar/baz}   -> {/ foo bar baz}
    test file::split.1.4 {file::split foo/bar/baz}      -> {foo bar baz}
    test file::split.1.5 {file::split ./foo/bar/baz}    -> {. foo bar baz}
    test file::split.1.6 {file::split ../foo/bar/baz}   -> {.. foo bar baz}
    test file::split.1.7 {file::split .../foo/bar/baz}  -> {... foo bar baz}
    test file::split.1.8 {
        file::split тест/こんにちは/世界
    } -> {тест こんにちは 世界}

    test file::split.2.1 {file::split {}}       -> {}
    test file::split.2.2 {file::split {     }}  -> {{     }}
    test file::split.2.3 {file::split .}    -> .
    test file::split.2.4 {file::split ..}   -> ..
    test file::split.2.5 {file::split ...}  -> ...
    test file::split.2.6 {file::split /}    -> /
    test file::split.2.7 {file::split ///}  -> /

    test file::split.3.1 {
        file::split {   /foo/bar/baz      }
    } -> {{   } foo bar {baz      }}
    test file::split.3.2 {file::split { }}  -> {{ }}

    test file::split.4 {
        file::split 0/1/2/3/4/5/6/7/8/9/a/b/d/e/f/ 10
    } -> {aborted: path has too many parts}

    test file::split.5.1 {file::split /foo/~bar/baz}    -> {/ foo ./~bar baz}
    test file::split.5.2 {file::split ~foo}             -> ~foo
    test file::split.5.3 {file::split ~~~foo}           -> ~~~foo
    test file::split.5.4 {file::split /foo/~bar/baz}    -> {/ foo ./~bar baz}
    test file::split.5.5 {
        file::split /~foo/~bar/~baz
    }  -> {/ ./~foo ./~bar ./~baz}
    test file::split.5.6 {file::split /foo/~~~bar/baz}  -> {/ foo ./~~~bar baz}
    test file::split.5.7 {file::split ~foo/~bar/baz}    -> {~foo ./~bar baz}


    test string::bytefirst-1.1 -constraints jim {
        lib::string::bytefirst c abcdef
    } -> 2
    test string::bytefirst-1.2 -constraints jim {
        lib::string::bytefirst f abcdef
    } -> 5
    test string::bytefirst-1.3 -constraints jim {
        lib::string::bytefirst world helloworld
    } -> 5
    test string::bytefirst-1.4 -constraints jim {
        lib::string::bytefirst е тест
    } -> 2
    test string::bytefirst-1.5 -constraints jim {
        lib::string::bytefirst тест мегатест
    } -> 8


    test named-args-1.1 {
        set args {-foo 1 -bar 5}
        lib::named-args {
            -foo  foo
            -bar  bar
            -baz  {baz default}
        }
        return [list $foo $bar $baz]
    } -> {1 5 default}

    test named-args-1.2.1 {
        set args {-foo 1 -bar 5}
        lib::named-args {
            -foo  foo
            -bar  bar
            -baz  baz
        }
    } -> {missing required argument -baz}

    test named-args-1.2.2 {
        set args {-foo 1}
        lib::named-args {
            -foo  foo
            -bar  bar
            -baz  baz
        }
    } ->$ {missing required argument -ba. -ba.}

    test named-args-1.3.1 {
        set args {-foo 1 -bar 5 -qux wat}
        lib::named-args {
            -foo  foo
            -bar  bar
        } 1
    } -> {unknown extra arguments: -qux}

    test named-args-1.3.2 {
        set args {-foo 1 -bar 5 -qux wat}
        lib::named-args {
            -foo  foo
            -bar  bar
        }
    } -> {unknown extra arguments: {-qux wat}}

    test named-args-1.4 {
        set args {-foo a b c}
        list [lib::named-args {
            -foo  {foo {default foo} flag}
        } 1] $foo
    } -> {{a b c} {default foo}}

    test named-args-1.5 {
        set args {--foo xyzzy}
        lib::named-args {
            -f     -
            -foo   -
            --foo  var
        } 1
        set var
    } -> xyzzy

    test named-args-1.6 {
        set args -help
        lib::named-args {-foo bar -baz qux}
    } -> {-foo bar -baz qux}


    cd [file dirname [info script]]

    test file::find-1.1 {
        lsort [file::find find-test-data]
    } -> "find-test-data/a/1\
          find-test-data/b/2\
          find-test-data/b/d/4\
          find-test-data/b/e/5\
          find-test-data/b/e/f/.6\
          find-test-data/b/e/f/g/7\
          find-test-data/c/.3"

    test file::find-1.2 {
        set found {}
        file::find find-test-data -filescript {
            lappend found [list $file $depth]
            list
        }

        lsort -index 1 $found
    } -> "{find-test-data/a/1 2}\
          {find-test-data/b/2 2}\
          {find-test-data/c/.3 2}\
          {find-test-data/b/d/4 3}\
          {find-test-data/b/e/5 3}\
          {find-test-data/b/e/f/.6 4}\
          {find-test-data/b/e/f/g/7 5}"

    test file::find-1.3 {
        set found {}
        file::find find-test-data -filescript {
            lappend found [list $file $depth]
            list
        } -dirfilter {[file readable $dir] && $depth < 2}

        lsort $found
    } -> {{find-test-data/a/1 2} {find-test-data/b/2 2} {find-test-data/c/.3 2}}

   test file::find-1.4 {
        set found [file::find find-test-data \
            -filevarname f \
            -depthvarname d \
            -filefilter {[regexp {^[^/]+/./.$} $f]} \
            -filescript { list $f $d } \
        ]

        lsort $found
    } -> {{find-test-data/a/1 2} {find-test-data/b/2 2}}

   test file::find-1.5 {
        set found [file::find find-test-data \
            -filevarname f \
            -filescript { file tail $f } \
        ]

        lsort $found
    } -> {.3 .6 1 2 4 5 7}


    set correctSeconds [clock scan {2014-06-26-20-10-00} \
            -format {%Y-%m-%d-%H-%M-%S}]
    set correctSecondsShort [clock scan {2014-01-01-00-00-00} \
            -format {%Y-%m-%d-%H-%M-%S}]

    test clock::scan-incremental-1.1 {
        clock::scan-incremental {2014-06-26 20:10}
    } -> [list $correctSeconds %Y-%m-%dT%H:%M]

    test clock::scan-incremental-1.2 {
        clock::scan-incremental {2014-06-26T20:10}
    } -> [list $correctSeconds %Y-%m-%dT%H:%M]

    test clock::scan-incremental-1.3 {
        clock::scan-incremental {2014 06 26 20 10}
    } -> [list $correctSeconds {%Y-%m-%dT%H:%M}]

    test clock::scan-incremental-1.4 {
        clock::scan-incremental {2014/06/26 20:10}
    } -> [list $correctSeconds {%Y-%m-%dT%H:%M}]

    test clock::scan-incremental-2.1 {
        clock::scan-incremental 2014
    } -> [list $correctSecondsShort %Y]

    test clock::scan-incremental-2.2 {
        clock::scan-incremental 2014-01
    } -> [list $correctSecondsShort %Y-%m]

    test clock::scan-incremental-2.3 {
        clock::scan-incremental {2014-01-01 00:00:00}
    } -> [list $correctSecondsShort {%Y-%m-%dT%H:%M:%S}]


    test clock::add-1.1 -constraints jim {
        clock::add 0 5 seconds
    } -> 5

    test clock::add-1.2 -constraints jim {
        clock::add 1 5 min
    } -> 301

    test clock::add-1.3 -constraints jim {
        clock::add 1 1 hour
    } -> 3601

    test clock::add-1.4 -constraints jim {
        clock::add 57 1 day
    } -> 86457

    test clock::add-1.5 -constraints jim {
        clock::add 0 1 day -12 hours -660 minutes -59 minutes -59 seconds
    } -> 1

    test clock::add-1.6 -constraints jim {
        clock::add 536526000 1 wk
    } -> 537130800

    test clock::add-1.7 -constraints jim {
        clock::add 536526000 4 wks
    } -> 538945200

    test clock::add-2.1 -constraints jim {
        expr {abs([clock::add 1575222308 1 month] - 1577900713) < 60}
    } -> 1

    test clock::add-2.2 -constraints jim {
        expr {abs([clock::add 1575222308 3 months] - 1583084747) < 60}
    } -> 1

    test clock::add-2.3 -constraints jim {
        clock::add 536526000 6 months
    } -> 552164400

    test clock::add-2.4 -constraints jim {
        clock::add 536526000 1 year
    } -> 568062000

    test clock::add-2.5 -constraints jim {
        clock::add 1000000000 10 years
    } -> 1315532800

    test clock::add-2.6 -constraints {64-bit jim} {
        clock::add -2208963600 100 years
    } -> 946710000

    test clock::add-3.1 -constraints jim {
        expr {abs(1575222308 - [clock::add 1577900713 -1 month]) < 60}
    } -> 1

    test clock::add-3.2 -constraints jim {
        expr {abs(1575222308 - [clock::add 1583084747 -3 months]) < 60}
    } -> 1

    test clock::add-3.3 -constraints jim {
        clock::add 552164400 -6 months
    } -> 536526000

    test clock::add-3.4 -constraints jim {
        clock::add 568062000 -1 year
    } -> 536526000

    test clock::add-3.5 -constraints jim {
        clock::add 1315532800 -10 years
    } -> 1000000000

    test clock::add-3.6 -constraints {64-bit jim} {
        clock::add 946710000 -100 years
    } -> -2208963600

    test clock::add-3.7 -constraints jim {
        clock::add 5 -5 seconds
    } -> 0

    test clock::add-3.8 -constraints jim {
        clock::add 301 -5 min
    } -> 1

    test clock::add-3.9 -constraints jim {
        clock::add 3601 -1 hour
    } -> 1

    test clock::add-3.10 -constraints jim {
        clock::add 86457 -1 day
    } -> 57


    test set::sym-diff-1.1 {
        set::sym-diff {a b c} {}
    } -> {{a b c} {}}

    test set::sym-diff-1.2 {
        set::sym-diff {} {a b c}
    } -> {{a b c} {}}

    test set::sym-diff-1.3 {
        set::sym-diff {a b c d e} {b c}
    } -> {{a d e} {b c}}

    test set::sym-diff-1.4 {
        set::sym-diff {a b c} {a b c}
    } -> {{} {a b c}}

    test set::sym-diff-1.5 {
        set::sym-diff {} {}
    } -> {{} {}}


    test set::subset?-1.1 {set::subset? {a b c} {a b c}} -> 1
    test set::subset?-1.2 {set::subset? {} {}} -> 1
    test set::subset?-1.3 {set::subset? {a b c} {}} -> 0
    test set::subset?-1.4 {set::subset? {} {a b c}} -> 1
    test set::subset?-1.5 {set::subset? {b e} {a b c d e f}} -> 1
    test set::subset?-1.6 {set::subset? {a x b} {a b c d e f}} -> 0


    test set::diff-1.1 {set::diff {a b c} {a b c}} -> {}
    test set::diff-1.2 {set::diff {} {}} -> {}
    test set::diff-1.3 {set::diff {a b c} {}} -> {a b c}
    test set::diff-1.4 {set::diff {} {a b c}} -> {}
    test set::diff-1.5 {set::diff {b e} {a b c d e f}} -> {}
    test set::diff-1.6 {set::diff {a x b} {a b c d e f}} -> x
    test set::diff-1.7 {set::diff {a b c d e f} {b e}} -> {a c d f}


    test log-1.1 {log discard hello} -> {}
    test log-1.2 {
        log info hello -configure {level err}
    } ->* {wrong # args*}
    test log-1.3 {
        set f [file tempfile]

        try {
            set ch [open $f w]
            variable log::config
            set config [dict merge $config [list channel $ch level debug]]
            log debug test
            log debug {test 2}
            close $ch

            set ch [open $f r]
            read $ch
        } finally {
            catch { close $ch }
            file delete $f
        }
    } ->* {==*(debug) test*==*(debug) {test 2}*}


    test pipe-1.1 {
        pipe x {list foo bar baz} {lindex $x 1}
    } -> bar

    test pipe-1.2 {
        pipe x {list foo bar baz} -map {lindex $x 0}
    } -> {foo bar baz}

    test pipe-1.3 {
        pipe x {list foo bar baz} -map {string reverse $x}
    } -> {oof rab zab}

    test pipe-1.4 -constraints jim {
        pipe x {range 0 101} -reduce acc 0 {expr {$x + $acc}}
    } -> 5050

    test pipe-1.5 {
        pipe x {list foo bar baz} -flatMap {split $x {}}
    } -> {f o o b a r b a z}

    test pipe-1.6 {
        pipe x {list 0 0 5 0 0 1 1 1 1 -99 108} -filter {$x > 0}
    } -> {5 1 1 1 1 108}

    test pipe-1.7 {
        pipe x {
            list 1foo foo1 1bar bar1 {} 111bar11 foo
        } -filter {
            [string match *foo* $x]
        }
    } -> {1foo foo1 foo}

    test pipe-1.8 {
        pipe x {
            list 1x 2x 3x xx 4x 5x yy 6x 7x
        } -map {
            if {![regexp (\\d) $x _ n]} continue
            lindex $n
        }
    } -> {1 2 3 4 5 6 7}

    test pipe-1.9 {
        pipe x {
            list foo bar baz ENOUGH junk 23o432
        } -map {
            if {$x eq {ENOUGH}} break
            lindex $x
        }
    } -> {foo bar baz}

    test pipe-1.10 {
        pipe x {
            list 1 2 3 4 a b c 5
        } -mapExpr {
            [string is integer $x] ? $x * 2 + 1 : 0
        }
    } -> {3 5 7 9 0 0 0 11}

    test pipe-1.11 {
        set result {}

        list [pipe x {
            list 1 2 3 4 5
        } -each {
            lappend result $x$x
        }] $result
    } -> {{} {11 22 33 44 55}}

    test pipe-1.12 {
        pipe x -init {foo bar baz} {lindex $x 1}
    } -> bar

    test pipe-1.13 {
        pipe x {list foo} -value bar
    } -> bar

    test pipe-1.14 {
        pipe x -initial {foo bar baz} -map {string reverse $x}
    } -> {oof rab zab}

    test pipe-1.15.1 {
        pipe x { lindex "foo\nbar\nbaz" } -eachline { string reverse $x }
    } -> {}

    test pipe-1.15.2 {
        pipe x { lindex "foo\nbar\nbaz" } -maplines { string reverse $x }
    } -> {oof rab zab}

    test pipe-1.15.3 {
        pipe x { lindex "foo\nbar\nbaz\n" } -maplines { string reverse $x }
    } -> {oof rab zab {}}

    test pipe-1.16.1 {
        pipe x { lindex "foo:bar:baz" } -eachsep : { string reverse $x }
    } -> {}

    test pipe-1.16.2 {
        pipe x { lindex "foo:bar:baz" } -mapsep : { string reverse $x }
    } -> {oof rab zab}

    test pipe-1.16.3 {
        pipe x { lindex "foo:bar:baz:" } -mapsep : { string reverse $x }
    } -> {oof rab zab {}}

    test pipe-1.17.1 {
        pipe x { list 5 1 4 2 3 } -sort { set x }
    } -> {1 2 3 4 5}

    test pipe-1.17.2 {
        pipe x { list aaaaa a aaaa aa aaa } -sort { string length $x }
    } -> {a aa aaa aaaa aaaaa}

    test pipe-1.18.1 {
        pipe _ -init { foo bar baz } -partition 1
    } -> {foo bar baz}

    test pipe-1.18.2 {
        pipe _ -init { foo bar baz } -partition 2
    } -> {{foo bar} baz}

    test pipe-1.18.3 {
        pipe _ -init { foo bar baz } -partition 3
    } -> {{foo bar baz}}

    test pipe-1.18.4 {
        pipe _ -init { foo bar baz } -partition 4
    } -> {{foo bar baz}}

    test pipe-1.18.5 {
        pipe _ -init { foo bar baz } -partition 0
    } -> {can't partition by 0 < 1}

    test pipe-1.19.1 {
        pipe _ -init {{foo bar} {baz qux} 0} -flatten 0
    } -> {{foo bar} {baz qux} 0}

    test pipe-1.19.2 {
        pipe _ -init {{foo bar} {baz qux} 0} -flatten 1
    } -> {foo bar baz qux 0}

    test pipe-1.19.3 {
        pipe _ -init {{foo bar} {baz qux} 0} -flatten 2
    } -> {foo bar baz qux 0}

    test pipe-1.19.4 {
        pipe _ -init {{foo {bar baz}} {qux 0}} -flatten 1
    } -> {foo {bar baz} qux 0}

    test pipe-1.19.5 {
        pipe _ -init {{foo {bar baz}} {qux 0}} -flatten 2
    } -> {foo bar baz qux 0}


    test pipe-2.1 -constraints jim {
        pipe _ {
            range 1 6
        } -map {
            string repeat # $_
        } -reduce s {} {
            append s $_\n
        }
    } -> #\n##\n###\n####\n#####\n

    test pipe-2.2.1 {
        pipe _ -reduce
    } -> {too few arguments for option -reduce}

    test pipe-2.2.2 {
        pipe _ -reduce x
    } -> {too few arguments for option -reduce}

    test pipe-2.2.3 {
        pipe _ -reduce x y
    } -> {too few arguments for option -reduce}

    test pipe-2.2.4 {
        pipe _ -reduce x y {}
    } -> y

    test pipe-2.3 {
        pipe _ -map
        } -> {too few arguments for option -map}

    test pipe-2.4 {
        pipe x { lindex 5 } -initial 7 { lindex $x }
    } -> {-initial used not as first argument}

    test pipe-2.5 {
        pipe x -initial 7
    } -> 7


    test string::indent-1.1 {
        string::indent 1\n2\n3 {    }
    } -> "    1\n    2\n    3"

    test string::indent-1.2 {
        string::indent 1\n2\n3\n \t
    } -> \t1\n\t2\n\t3\n\t


    test template::eval-1.1 {
        template::eval {A<%= $foo %>C} {foo B}
    } -> ABC

    test template::eval-1.2 {
        set a 7
        template::eval {<% upvar 1 a a %><%= $a %>}
    } -> 7


    test file::reroot-1.1 {file::reroot /foo /xxx /foo/bar/baz} -> /xxx/bar/baz
    test file::reroot-1.2 {file::reroot /foo/bar /xxx /foo/bar/baz} -> /xxx/baz
    test file::reroot-1.3 {file::reroot /foo/bar/baz /xxx /foo/bar/baz} -> /xxx
    test file::reroot-1.4 {file::reroot / {} /foo/bar/baz} -> foo/bar/baz
    test file::reroot-1.5 {file::reroot foo xxx foo/bar/baz} -> xxx/bar/baz
    test file::reroot-1.6 {file::reroot foo/bar xxx foo/bar/baz} -> xxx/baz
    test file::reroot-1.7 {file::reroot foo/bar/baz xxx foo/bar/baz} -> xxx
    test file::reroot-1.8 {
        file::reroot /nope / /foo/bar/baz
    } -> {/foo/bar/baz not in /nope}


    test file::with-path-1.1 {
        file::with-path find-test-data/b/ {
            file isfile 2
        }
    } -> 1

    test file::with-path-1.2 {
        file::with-path find-test-data/b/ {}
        file isdir find-test-data
    } -> 1


    test file::list-dir-1.1 {
        file::list-dir find-test-data/
    } -> {find-test-data/a find-test-data/b find-test-data/c}
    test file::list-dir-1.2 {
        file::list-dir find-test-data/c/
    } -> find-test-data/c/.3
    test file::list-dir-1.3 {
        file::list-dir find-test-data/b/e/f/
    } -> {find-test-data/b/e/f/.6 find-test-data/b/e/f/g}
    test file::list-dir-1.4 {
        try {
            set prev [pwd]
            cd find-test-data/b/e/f
            file::list-dir
        } finally {
            cd $prev
        }
    } -> {.6 g}

    test file::list-dir-2.1 {
        file::list-dir -tails find-test-data/
    } -> {a b c}
    test file::list-dir-2.2 {
        file::list-dir -tails find-test-data/c/
    } -> .3
    test file::list-dir-2.3 {
        file::list-dir -tails find-test-data/b/e/f/
    } -> {.6 g}
    test file::list-dir-2.4 {
        try {
            set prev [pwd]
            cd find-test-data/b/e/f
            file::list-dir -tails
        } finally {
            cd $prev
        }
    } -> {.6 g}
    test file::list-dir-2.5 {
        file::list-dir -- -tails find-test-data/b/e/f/
    } ->* {wrong # args*}
    test file::list-dir-2.6 {
        file::list-dir -tails -- find-test-data/b/e/f/
    } -> {.6 g}

    test file::list-dir-3.1 -constraints jim {
        file mkdir empty/
        defer {file delete empty/}
        file::list-dir empty
    } -> {}


    test try-1.1 {try {lindex foo}} -> foo
    test try-1.2 {try {lindex foo} on ok _ {lindex bar}} -> bar
    test try-1.3 {try {lindex foo} on ok x {lindex $x}} -> foo
    test try-1.4 {try {lindex foo} on error _ {lindex bar}} -> foo
    test try-1.5 {try {lindex foo} on return _ {lindex bar}} -> foo
    test try-1.6 {try {lindex foo} on break _ {lindex bar}} -> foo
    test try-1.7 {try {lindex foo} on continue _ {lindex bar}} -> foo
    test try-1.8 {try {lindex foo} on # _ {lindex bar}} ->* {bad code '#'*}

    test try-2.1 {try {error nope} on error _ {lindex bar}} -> bar
    test try-2.2 {try {error nope} on break _ {lindex bar}} -> nope
    test try-2.3 {try break on break _ {lindex bar}} -> bar
    test try-2.4 {try continue on break _ {lindex bar}} -> {}
    test try-2.5 {try continue on continue _ {lindex bar}} -> bar
    test try-2.6 {try {return nope} on continue _ {lindex bar}} -> nope
    test try-2.7 {try {return nope} on return _ {lindex bar}} -> bar

    test try-3.1 {
        try {
            apply {{} {return -code error -errorcode {FOO BAR} failed}}
        } trap FOO {msg opts} {
            list trapped
        }
    } -> trapped

    test try-3.2 {
        try {
            apply {{} {return -code error -errorcode {FOO BAR} failed}}
        } trap {FOO BAR} {msg opts} {
            list trapped
        }
    } -> trapped

    test try-3.3 {
        try {
            apply {{} {return -code error -errorcode {FOO BAR} failed}}
        } trap BAZ {msg opts} {
            list trapped
        }
    } -> failed

    test try-3.4 {
        try {
            apply {{} {return -code error -errorcode {FOO BAR} failed}}
        } trap {FOO BAZ} {msg opts} {
            list trapped
        }
    } -> failed


    test list::lcs-1.1 -constraints jim {
        list::lcs {a b c} {a b c}
    } -> {{0 1 2} {0 1 2}}
    test list::lcs-1.2 -constraints jim {
        list::lcs {a b c} {a c b}
    } -> {{0 1} {0 2}}
    test list::lcs-1.3 -constraints jim {
        list::lcs {1 h e l l o 3 0} {2 w o r l d 4 5 6 0}
    } -> {{3 7} {4 9}}
    test list::lcs-1.4 -constraints jim {
        set list1 {b c d e f g h}
        set list2 {a c d g i j k}
        lassign [list::lcs $list1 $list2] indices1 indices2
        list [list::pick $list1 $indices1] [list::pick $list2 $indices2]
    } -> {{c d g} {c d g}}

    test list::lcs-2.1 -constraints jim {list::lcs {} {a c b}} -> {{} {}}
    test list::lcs-2.2 -constraints jim {list::lcs {a b c} {}} -> {{} {}}
    test list::lcs-2.3 -constraints jim {list::lcs {} {}} -> {{} {}}


    test list::pick-1.1 {list::pick {a b c d e f g h} {0 0 0 5}} -> {a a a f}
    test list::pick-1.2 {list::pick {a b c d e f g h} {}} -> {}


    test list::diff-1.1 -constraints jim {list::diff {} {}} -> {}
    test list::diff-1.2 -constraints jim {list::diff foo {}} -> {- foo}
    test list::diff-1.3 -constraints jim {list::diff {} bar} -> {+ bar}
    test list::diff-1.4 -constraints jim {list::diff foo bar} -> {- foo + bar}

    test list::diff-1.5 -constraints jim {
        list::diff {bar baz} {foo bar baz}
    } -> {+ foo = bar = baz}

    test list::diff-1.6 -constraints jim {
        list::diff {foo baz} {foo bar baz}
    } -> {= foo + bar = baz}

    test list::diff-1.7 -constraints jim {
        list::diff {foo bar} {foo bar baz}
    } -> {= foo = bar + baz}

    test list::diff-1.8 -constraints jim {
        list::diff {a c f} {a b c d e f}
    } -> {= a + b = c + d + e = f}

    test list::diff-1.9 -constraints jim {
        list::diff {a b c d e f} {a c f}
    } -> {= a - b = c - d - e = f}

    test list::diff-1.10 -constraints jim {
        list::diff {0 1 2 3} {a b c 0 e f g}
    } -> {+ a + b + c = 0 - 1 - 2 - 3 + e + f + g}


    test list::transpose-1.1 {list::transpose {}} -> {}
    test list::transpose-1.2 {list::transpose {1 2 3}} -> {{1 2 3}}
    test list::transpose-1.3 {list::transpose {{a b} {c d}}} -> {{a c} {b d}}
    test list::transpose-1.4 {list::transpose {{} {a c d}}} -> {}
    test list::transpose-1.5 {
        list::transpose {{a b 0} {c d}}
    } -> {{a c} {b d}}

    test list::zip-1.1 {
        list::zip {0 1 2} {3 4 5} {6 7 8}
    } -> {{0 3 6} {1 4 7} {2 5 8}}


    test file::tar::header-1.1 {
        file::tar::header -filename hello.txt -size 42
    } ->* hello.txt*

    test file::tar::header-2.1 {
        file::tar::header -filename [string repeat X 100] -size 0
    } -> {field filename exceeds 99 bytes}

    test file::tar::write-1.1 -constraints {!openbsd tar} {
        set f [file tempfile]

        try {
            set ch [open $f w]
            file::tar::write $ch {Hello, world!} \
                                 -filename {dir/local greeting.txt} \
                                 -mtime 1576740678
            file::tar::write $ch {Hello, universe! ... ... ... Aargh!} \
                                 -filename dir/careful.txt \
                                 -uname foo \
                                 -gname bar \
                                 -mtime 0
            file::tar::finish $ch
            close $ch

            exec tar tvf $f
        } finally {
            catch { close $ch }
            file delete $f
        }
    } ->* "-rw-r--r--*root*13*2019*dir/local\
           greeting.txt*-rw-r--r--*foo*bar*35*1970*dir/careful.txt*"

    test file::tar::write-1.2 -constraints tar {
        set f [file tempfile]
        set testFile foo-tar-test-file

        try {
            set ch [open $f w]
            file::tar::write $ch \
                \x12\x34bar\x56\x78 \
                -filename $testFile \
                -mtime 1576740678
            file::tar::finish $ch
            close $ch

            exec tar -x -f $f

            set ch [open $testFile]
            set content [read $ch]
            close $ch

            set content
        } finally {
            catch { close $ch }
            file delete $f
            file delete $testFile
        }
    } -> \x12\x34bar\x56\x78

    test file::tar::write-2.1 -constraints {gzip tar} {
        set f [file tempfile]

        try {
            set ch [open [list | gzip > $f] w]
            file::tar::write $ch {Hello, world!} \
                                 -filename hello.txt \
                                 -mtime 1576740678
            file::tar::finish $ch
            close $ch

            exec tar ztvf $f
        } finally {
            catch { close $ch }
            file delete $f
        }
    } ->* *root*hello.txt*


    test term::ansi::attrs-1.1 {
        term::ansi::attrs
    } -> \033\[m

    test term::ansi::attrs-1.2 {
        term::ansi::attrs {bg black} \
                        {fg green} \
                        bold \
                        italic \
                        {underline double} \
                        strikethrough
    } -> \033\[40\;32\;1\;3\;21\;9m

    test term::ansi::attrs-1.3 {
        term::ansi::attrs {fg 8-bit 42} {bg rgb 1 2 3}
    } -> \033\[38\;5\;42\;48\;2\;1\;2\;3m

    test term::ansi::rgb5-to-8-bit-1.1 {
        list [term::ansi::rgb5-to-8-bit 5 0 0] \
             [term::ansi::rgb5-to-8-bit 0 5 0] \
             [term::ansi::rgb5-to-8-bit 0 0 5] \
             [term::ansi::rgb5-to-8-bit 5 5 0] \
             [term::ansi::rgb5-to-8-bit 0 5 5] \
             [term::ansi::rgb5-to-8-bit 5 0 5]
    } -> {196 46 21 226 51 201}

    test term::ansi::rgb5-to-8-bit-2.1 {
        term::ansi::rgb5-to-8-bit 0 6 0
    } -> {rgb values {0 6 0} not in range of 0 to 5}


    test list::sl-1.1 -constraints jim {
        list::sl {1 2 3 $(2 + 2) [lindex 5]}
    } -> {1 2 3 4 5}

    test list::sl-1.2 {
        list::sl {
            foo  ;# bar
            baz
        }
    } -> {foo baz}

    test list::sl-2.1 {
        list::sl foo {}
    } -> {invalid command name "foo"}

    test list::sl-2.2 {
        set i 0
        list::sl {
            incr i  ;# foo
            incr i
            incr i
        } {}
    } -> {1 2 3}


    set range {}
    for {set i 0} {$i < 100} {incr i} { lappend range $i }
    test mp::pmap-1.1 -constraints jim {
        mp::pmap x [range 0 100] {lindex $x} 20
    } -> $range
    unset range

    test mp::pmap-1.2 -constraints jim {
        mp::pmap {x y} [range 0 10] {expr {$x + $y}} 2
    } -> {1 5 9 13 17}

    test mp::pmap-2.1 -constraints jim {
        mp::pmap x [range 0 100] {lindx $x} 20
    } ->$ [string cat {child process \d+ for chunk 0 to 19 } \
                      {exited with status 1 and message } \
                      {\{invalid command name "lindx"\}}]


    test csv::split-1.1 {csv::split 1,2,3,4,5} -> {1 2 3 4 5}
    test csv::split-1.2 {csv::split {"1","2","3","4","5"}} -> {1 2 3 4 5}
    test csv::split-1.3 -constraints jim {
        csv::split {"aaa","b""bb","ccc"}
    } -> {aaa {b"bb} ccc}
    test csv::split-1.4 {
        csv::split {"Hello, world!",37,foo}
    } -> {{Hello, world!} 37 foo}
    test csv::split-1.5 {
        csv::split "\"aaa\",\"b\n\n\nbb\",\"ccc\""
    } -> "aaa {b\n\n\nbb} ccc"
    # How the following case should be handled is ambiguous. The test is
    # documentation, not a prescription. {1 { "2"} {"3" } { "4" } {   "5"  }}
    # (i.e., ignoring double quotes when there aren't two of them adjacent to
    # the separators around the value) would also be a reasonable result.
    test csv::split-1.6 {
        csv::split {"1", "2","3" , "4" ,   "5"  }
    } -> {1 { 2} {3 } { 4 } {   5  }}
    test csv::split-1.7 {csv::split 1,,,,5} -> {1 {} {} {} 5}
    test csv::split-1.8 {csv::split {",",",",","}} -> {, , ,}
    test csv::split-1.9 {csv::split {}} -> {}
    test csv::split-1.10 {csv::split {   }} -> {{   }}

    test csv::split-2.1 {csv::split foo\t\tbar\tbaz {\t}} -> {foo {} bar baz}


    test hash::mult-1.1 -constraints jim {
        format %x [hash::mult a000]
    } -> 17c9312d6
    test hash::mult-1.2 -constraints jim {
        format %x [hash::mult a000 0 31]
    } -> 2cd22f
    test hash::mult-1.3 -constraints jim {
        format %x $([hash::mult {Hello, world!}] & 0xffffffff)
    } -> e18796ae
    test hash::mult-1.4 -constraints jim {
        format %x $([hash::mult {Hello, world!} 0 31] & 0xffffffff)
    } -> 8ff0cbf5
    test hash::mult-1.5 -constraints jim {
        hash::mult {why so negative, huh?}
    } -> -2972932130062078


    test checksum::parse::line-1.1 {
        checksum::parse::line \
            {b40e0f641d786c71a48fc36476f2d3c8  jimlib.tcl}
    } -> {? jimlib.tcl b40e0f641d786c71a48fc36476f2d3c8}

    test checksum::parse::line-1.2 {
        checksum::parse::line \
            "b40e0f641d786c71a48fc36476f2d3c8 *jimlib.tcl"
    } -> {? jimlib.tcl b40e0f641d786c71a48fc36476f2d3c8}

    test checksum::parse::line-1.3 {
        checksum::parse::line \
            "B40E0F641D786C71A48FC36476F2D3C8   \t  jimlib.tcl"
    } -> {? jimlib.tcl B40E0F641D786C71A48FC36476F2D3C8}

    test checksum::parse::line-1.4 {
        checksum::parse::line \
            {\d41d8cd98f00b204e9800998ecf8427e  a\nb}
    } -> "? {a\nb} d41d8cd98f00b204e9800998ecf8427e"

    test checksum::parse::line-2.1 {
        checksum::parse::line blah
    } -> {can't parse line blah}

    test checksum::parse::tagged-line-1.1 {
        checksum::parse::tagged-line \
            "SHA3-224 (jimlib.tcl) =\
             2727b06b1fab7cec39d0d95596e2b7486207740541c09c8c72c972f3"
    } -> "SHA3-224\
          jimlib.tcl\
          2727b06b1fab7cec39d0d95596e2b7486207740541c09c8c72c972f3"

    test checksum::parse::tagged-line-1.2 {
        checksum::parse::tagged-line \
            "SHA3-224   (jimlib.tcl)  =  \t  \
             2727B06B1FAB7CEC39D0D95596E2B7486207740541C09C8C72C972F3"
    } -> "SHA3-224\
          jimlib.tcl\
          2727B06B1FAB7CEC39D0D95596E2B7486207740541C09C8C72C972F3"

    test checksum::parse::tagged-line-1.3 {
        checksum::parse::tagged-line \
            {\MD5 (a\nb) = d41d8cd98f00b204e9800998ecf8427e}
    } -> "MD5 {a\nb} d41d8cd98f00b204e9800998ecf8427e"

    test checksum::parse::tagged-line-1.4 {
        checksum::parse::tagged-line \
            {MD5 (Image(927).jpg) = 2f008c8309322d7ef872814746f33979}
    } -> {MD5 Image(927).jpg 2f008c8309322d7ef872814746f33979}

    test checksum::parse::tagged-line-1.5 {
        checksum::parse::tagged-line \
            {MD5 (abc(def) = 2f008c8309322d7ef872814746f33979}
    } -> {MD5 abc(def 2f008c8309322d7ef872814746f33979}

    test checksum::parse::tagged-line-1.6 {
        checksum::parse::tagged-line \
            {MD5 (abcdef) = 2f00) = 2f008c8309322d7ef872814746f33979}
    } -> {MD5 {abcdef) = 2f00} 2f008c8309322d7ef872814746f33979}

    test checksum::parse::tagged-line-2.1 {
        checksum::parse::tagged-line blah
    } -> {can't parse line blah}

    test checksum::parse::many-1.1 {
        checksum::parse::many [join {
            {MD5 (Makefile) = 8da0c2013b7e5c4df41acc5dc55b4166}
            {MD5 (procs.tcl) = 39f17dd5517a0e68de0bc095164d8607}
            {ce6a6bb38843f66320dc04b094554d68  README.md}
        } \n]
    } -> "MD5 Makefile 8da0c2013b7e5c4df41acc5dc55b4166\
          MD5 procs.tcl 39f17dd5517a0e68de0bc095164d8607\
          ? README.md ce6a6bb38843f66320dc04b094554d68"

    test checksum::parse::many-2.1 {
        checksum::parse::many \n\t\n1\n2\n3\n
    } -> {can't parse line 1}


    test math::atan2-1.1 -constraints jim {math::atan2 1.0 2.0}   ->* 0.463647*
    test math::atan2-1.2 -constraints jim {math::atan2 0.0 1.0}   ->  0.0
    test math::atan2-1.3 -constraints jim {math::atan2 1.0 0.0}   ->* 1.570796*
    test math::atan2-1.4 -constraints jim {math::atan2 0.0 0.0}   ->  0.0
    test math::atan2-1.5 -constraints jim {math::atan2 0.25 0.75} ->* 0.321750*


    test math::base-n::from-decimal-1.1 {
        math::base-n::from-decimal {0 1 2 3 4 5 6 7 8 9} 12345990
    } -> 12345990

    test math::base-n::from-decimal-1.2 {
        math::base-n::from-decimal {0 1 2 3 4 5 6 7 8 9 a b c d e f} \
                                   [expr 0xffabcd]
    } -> ffabcd

    test math::base-n::from-decimal-1.3 {
        variable ulid::base32

        math::base-n::from-decimal $base32 1469918176385
    } -> 1ARYZ6S41


    test math::base-n::to-decimal-1.1 {
        math::base-n::to-decimal {0 1 2 3 4 5 6 7 8 9} 12345990
    } -> 12345990

    test math::base-n::to-decimal-1.2 {
        math::base-n::to-decimal {0 1 2 3 4 5 6 7 8 9 a b c d e f} ffabcd
    } -> [expr 0xffabcd]

    test math::base-n::to-decimal-1.3 {
        variable ulid::base32

        math::base-n::to-decimal $base32 01ARYZ6S41
    } -> 1469918176385


    test math::complex::+-1.1 -constraints jim {
        math::complex::+ {1 -2} {5 7}
    } -> {6 5}
    test math::complex::+-1.2 -constraints jim {
        math::complex::+ 1 2
    } -> {3 0}

    test math::complex::*-1.1 -constraints jim {math::complex::* 1 1} -> {1 0}
    test math::complex::*-1.2 -constraints jim {
        math::complex::* {0 1} {0 1}
    } -> {-1 0}
    test math::complex::*-1.3 -constraints jim {
        math::complex::* {2 3} {7 11}
    } -> {-19 43}

    test math::complex::mod-1.1 -constraints jim {math::complex::mod 1} -> 1.0
    test math::complex::mod-1.2 -constraints jim {
        math::complex::mod {0 1}
    } -> 1.0
    test math::complex::mod-1.3 -constraints jim {
        math::complex::mod {2 2}
    } ->* 2.828427*


    test string::match-bracket-1.1 {
        string::match-bracket ()()()
    } -> 1
    test string::match-bracket-1.2 {
        string::match-bracket {(1 + -2 * (3 + 4))}
    } -> 17
    test string::match-bracket-1.3 {
        string::match-bracket -start 3 {abs(1 + -2 * (3 + 4))}
    } -> 20
    test string::match-bracket-1.4 {
        string::match-bracket {(\()}
    } -> 3
    test string::match-bracket-1.5 {
        string::match-bracket -escape ^ {(^()}
    } -> 3

    test string::match-bracket-2.1 {
        string::match-bracket (
    } -> {unmatched brackets: 1}
    test string::match-bracket-2.2 {
        string::match-bracket (((
    } -> {unmatched brackets: 3}
    test string::match-bracket-2.3 {
        string::match-bracket ((((())))
    } -> {unmatched brackets: 1}
    test string::match-bracket-2.4 {
        string::match-bracket {}
    } -> {start isn't a bracket}
    test string::match-bracket-2.5 {
        string::match-bracket { ()()()}
    } -> {start isn't a bracket}


    test string::pandoc-id-1.1 {string::pandoc-id hello-world} -> hello-world
    test string::pandoc-id-1.2 {string::pandoc-id {Hello, world!!}} -> hello-world
    test string::pandoc-id-1.3 {string::pandoc-id {}} -> section
    test string::pandoc-id-1.4 {string::pandoc-id {foo   bar---baz}} -> foo-bar---baz
    test string::pandoc-id-1.5 {string::pandoc-id { [[[ }} -> section


    test string::slugify-1.1 {string::slugify hello-world} -> hello-world
    test string::slugify-1.2 {string::slugify {Hello, world!!}} -> hello-world


    test $-1.1 {set a 5; $ a} -> 5
    test $-1.2 {set a {k1 v1 k2 v2}; $ a} -> {k1 v1 k2 v2}
    test $-1.3 {set a {k1 v1 k2 v2}; $ a k1} -> v1

    test $-2.1 {$ a 5} -> {can't read "a": no such variable}
    test $-2.2 {set a {k1 v1}; $ a k2} -> {key "k2" not known in dictionary}


    test id-1.1 {id foo} -> foo
    test id-2.1 {id foo bar} -> {wrong # args: should be "id value"}


    test fill-repeats-1.1 {dict::fill-repeats {a b c d}} -> {a b c d}
    test fill-repeats-1.2 {dict::fill-repeats {a - c - e f}} -> {a f c f e f}
    test fill-repeats-1.3 {dict::fill-repeats {a - c - e -}} -> {a {} c {} e {}}

    test fill-repeats-2.1 {dict::fill-repeats no} -> {not a dictionary: no}


    test unindent-1.1 {
        string::unindent {}
    } -> {}

    test unindent-1.2 {
        list \
            [string::unindent " a\n b\n c"] \
            [string::unindent "    a\n    b\n    c"] \
            [string::unindent "\ta\n\tb\n\tc" -chars " \t"] \
    } -> [list a\nb\nc a\nb\nc a\nb\nc]

    test unindent-1.3 {
        list \
            [string::unindent " a\n  b\n c"] \
            [string::unindent "    a\n            b\n        c"] \
            [string::unindent "\ta\n\t\tb\n\tc" -chars " \t"] \
    } -> [list "a\n b\nc" "a\n        b\n    c" a\n\tb\nc]

    test unindent-1.5 {
        list \
            [string::unindent " a\n  b\n c\n"] \
            [string::unindent "    a\n            b\n        c\n"] \
            [string::unindent "\ta\n\t\tb\n\tc" -chars " \t\n"] \
    } -> [list "a\n b\nc" "a\n        b\n    c" a\n\tb\nc]

    test unindent-1.6 {
        list \
            [string::unindent "\n a\n  b\n c"] \
            [string::unindent "\n    a\n            b\n        c"] \
            [string::unindent "\n\ta\n\t\tb\n\tc" -chars " \t"] \
    } -> [list "a\n b\nc" "a\n        b\n    c" a\n\tb\nc]

    test unindent-1.7 {
        list \
            [string::unindent " a\n\n  b\n c"] \
            [string::unindent "    a\n            b\n\n        c"] \
            [string::unindent "\ta\n\n\t\tb\n\n\tc" -chars " \t"] \
    } -> [list "a\n\n b\nc" "a\n        b\n\n    c" a\n\n\tb\n\nc]

    test unindent-1.8 {
        list \
            [string::unindent "a  a\nb  b\nc  c"] \
            [string::unindent " a \n b \n c"] \
            [string::unindent "a     "] \
    } -> [list "a  a\nb  b\nc  c" "a \nb \nc" "a     "]

    test unindent-1.9 {
        string::unindent {
            foo
            bar
            baz
        }
    } -> foo\nbar\nbaz

    test unindent-1.10 {
        list \
            [string::unindent { }] \
            [string::unindent " \n "] \
            [string::unindent \nxx\nxxxx\nxx\nx -chars x] \
    } -> "{} {} {\n\n}"

    test unindent-1.11 {
        list \
            [string::unindent \nxx\nxxxx\nxx\nx \
                -chars x \
                -ignoreindentonly false \
            ] \
            [string::unindent ----A\n--\n----B \
                -chars - \
                -ignoreindentonly false \
            ] \
            [string::unindent ----A\n--\n----B \
                -chars - \
                -ignoreindentonly true \
            ] \
    } -> "{x\nxxx\nx\n} {--A\n\n--B} {A\n\nB}"

    test unindent-1.12 {
        list \
            [string::unindent "  a\n  b\n  c" -max 1] \
            [string::unindent "  a\n  b\n  c" -max 2] \
    } -> "{ a\n b\n c} {a\nb\nc}"


    test ulid-1.1 {
        ulid 1469918176385
    } ->$ {01ARYZ6S41[0-9A-Z]{16}}


    test dict::getdef-1.1 {
        dict::getdef {} someKey testValue
    } -> testValue

    test dict::getdef-1.2  {
        dict::getdef {someKey testValue} someKey -1
    } -> testValue

    test dict::getdef-1.3 {
        dict::getdef {someKey {anotherKey testValue}} \
            someKey anotherKey \
            -1
    } -> testValue

    test dict::getdef-2.1 {
        dict::getdef {k {k2 v}} k k3 -1
    } -> -1

    test dict::getdef-2.2 {
        dict::getdef {k {k2 v}} k k2 k3 -1
    } -> {missing value to go with key}


    set idProc [list proc lib::id value [info body lib::id]]

    test info::showproc-1.1 {
        info::showproc
    } -> {}

    test info::showproc-1.2 {
        info::showproc lib::id
    } -> $idProc

    test info::showproc-1.3 {
        lindex [info::showproc lib::clock::scan-incremental] 2
    } -> {date {options {}} {debug 0}}

    test info::showproc-1.4 {
        info::showproc lib::id lib::id
    } -> [list $idProc $idProc]

    test info::showproc-1.4 {
        info::showproc lib::id lib::id
    } -> [list $idProc $idProc]

    test info::showproc-1.5 {
        lrange [info::showproc lib::dict::getdef] 0 2
    } -> {proc lib::dict::getdef args}

    test info::showproc-1.6.1 -constraints jim {
        set static1 5
        local proc ::sptest x static1 {}
        info::showproc ::sptest
    } -> {proc ::sptest x {{static1 5}} {}}

    test info::showproc-1.6.2 -constraints jim {
        set static1 5
        local proc ::sptest x {static1 {static2 7}} {}
        info::showproc ::sptest
    } -> {proc ::sptest x {{static1 5} {static2 7}} {}}

    test info::showproc-2.1 {
        info::showproc {no such proc}
    } -> {{no such proc} isn't a procedure}

    test info::showproc-2.2 {
        info::showproc lindex
    } -> {lindex isn't a procedure}

    unset idProc


    test list::product-1.1 { list::product {} } -> {}
    test list::product-1.2 { list::product a } -> a
    test list::product-1.3 { list::product a b } -> {{a b}}

    test list::product-1.4 {
        list::product {a b} {c d}
    } -> {{a c} {a d} {b c} {b d}}

    test list::product-1.5 {
        list::product a {c d}
    } -> {{a c} {a d}}

    test list::product-1.6 {
        list::product {a b} c
    } -> {{a c} {b c}}

    test list::product-1.7 {
        list::product {a b c} {0 1 2} {x y z}
    } -> [list \
        {a 0 x} {a 0 y} {a 0 z} {a 1 x} {a 1 y} {a 1 z} {a 2 x} {a 2 y} \
        {a 2 z} {b 0 x} {b 0 y} {b 0 z} {b 1 x} {b 1 y} {b 1 z} {b 2 x} \
        {b 2 y} {b 2 z} {c 0 x} {c 0 y} {c 0 z} {c 1 x} {c 1 y} {c 1 z} \
        {c 2 x} {c 2 y} {c 2 z} \
    ]

    test list::product-1.8 {
        list::product {} {} a {} {}
    } -> {}

    test list::product-1.9 {
        list::product
    } -> {}


    set q {no queue created}
    set redisKey jimlib-test

    test queue-1.1 -constraints redis {
        upvar 1 q q
        upvar 1 r r
        upvar 1 redisKey redisKey

        set q [queue::new $r $redisKey]

        $q reset
    } -> {}

    test queue-1.2 -constraints redis {
        upvar 1 q q

        $q length
    } -> 0

    test queue-1.3 -constraints redis {
        upvar 1 q q

        $q add Test
        list [$q length] [$q get]
    } -> {1 Test}

    test queue-1.4 -constraints redis {
        upvar 1 q q

        $q add Foo
        $q add Bar Baz
        list [$q get] [$q get] [$q get]
    } -> {Foo Bar Baz}

    test queue-1.5 -constraints redis {
        upvar 1 q q

        $q add {*}[range 10]
        set list [$q list]
        $q reset

        return $list
    } -> {0 1 2 3 4 5 6 7 8 9}

    test queue-1.6 -constraints redis {
        upvar 1 q q

        $q add Foo
        list [$q empty?] [$q get] [$q empty?]
    } -> {0 Foo 1}

    test queue-1.7 -constraints redis {
        upvar 1 q q

        $q add
    } -> 0

    test queue-1.8 -constraints redis {
        upvar 1 q q

        $q add {Hello, worker!}
        queue::worker $q x {
            return -level 0 -code break $x
        }
    } -> {Hello, worker!}

    test queue-1.9 -constraints redis {
        upvar 1 q q

        $q add foo bar baz %END%

        queue::worker $q x {
            if {$x eq {%END%}} break

            lappend acc $x
        }

        return $acc
    } -> {foo bar baz}

    test queue-1.10 -constraints redis {
        upvar 1 q q

        $q add Hedwig
        try {
            $q head
        } finally {
            $q reset
        }
    } -> Hedwig


    test queue-2.1 -constraints redis {
        upvar 1 q q

        $q get
    } -> {empty queue}

    test queue-2.2 -constraints redis {
        upvar 1 q q

        $q head
    } -> {empty queue}


    test queue-3.1 -constraints redis {
        upvar 1 q q
        upvar 1 r r
        upvar 1 redisKey redisKey

        $q add Saved
        $q destroy

        list [$r lrange $redisKey 0 0] [$r del $redisKey]
    } -> {Saved 1}


    test namespace-ensemble-1.1 -constraints jim {
        try {
            namespace eval ::ns {}
            namespace::ensemble ::ns
        } finally {
            namespace delete ::ns
        }
    } -> ::ns

    test namespace-ensemble-1.2 -constraints jim {
        try {
            namespace eval ::ns {}
            namespace::ensemble ::ns

            proc ::ns::foo {} { return 5 }
            ::ns foo
        } finally {
            namespace delete ::ns
        }
    } -> 5


    test namespace-ensemble-2.1 -constraints jim {
        try {
            namespace eval ::ns {}
            namespace::ensemble ::ns

            proc ::ns::foo {} {}
            ::ns
        } finally {
            namespace delete ::ns
        }
    } -> {unknown or ambiguous subcommand "": must be foo}

    test namespace-ensemble-2.2 -constraints jim {
        try {
            namespace eval ::ns {}

            proc ::ns::foo {} {}
            proc ::ns::bar {} {}
            proc ::ns::baz {} {}

            namespace::ensemble ::ns
            ::ns x
        } finally {
            namespace delete ::ns
        }
    } -> {unknown or ambiguous subcommand "x": must be bar, baz, or foo}


    test oo-class-1.1 -constraints jim {
        set cls [oo::class class {}]
        namespace delete $cls
    } -> {}

    test oo-class-1.2 -constraints jim {
        set cls [oo::class class {}]

        try {
            namespace delete [${cls}::new]
        } finally {
            namespace delete $cls
        }
    } -> {}

    test oo-class-1.3 -constraints jim {
        set cls [oo::class class {
            variable _x
            constructor {foo bar baz} { set _x 7 }
            method x {} { return $_x }
        }]

        try {
            set obj [${cls}::new 1 2 3]
            $obj x
        } finally {
            catch { $obj destroy }
            namespace delete $cls
        }
    } -> 7

    test oo-class-1.3 -constraints jim {
        set cls [oo::class class {
            variable {contains a space}
            constructor {} { set {contains a space} yes }
            method x {} { return ${contains a space} }
        }]

        try {
            set obj [${cls}::new]
            $obj x
        } finally {
            catch { $obj destroy }
            namespace delete $cls
        }
    } -> yes

    test oo-class-1.4 -constraints jim {
        set cls [oo::class class {
            method {s p a c e} {{"the final frontier" true}} {
                return ${the final frontier}
            }
        }]

        try {
            set obj [${cls}::new]
            $obj {s p a c e}
        } finally {
            catch { $obj destroy }
            namespace delete $cls
        }
    } -> true

    test oo-class-1.5 -constraints jim {
        set cls [oo::class {space class} {
            method m {} { return working }
        } false]

        try {
            set obj [${cls}::new]
            $obj m
        } finally {
            catch { $obj destroy }
            namespace delete $cls
        }
    } -> working

    test oo-class-1.6 -constraints jim {
        set cls [oo::class custom-destructor {
            method destroy _ {
                namespace delete [namespace current]
                return [list deleted [namespace current]]
            }
        } false]

        try {
            set obj [${cls}::new]
            set result [$obj destroy !]
            list [info commands ${obj}::*] $result
        } finally {
            namespace delete $cls
        }
    } ->* {{} {deleted *oo::lib::custom-destructor::handle*}}


    test oo-class-2.1 -constraints jim {
        set cls [oo::class class {}]

        try {
            ${cls}::new x
        } finally {
            namespace delete $cls
        }
    } -> {wrong # args: should be "constructor"}

    test oo-class-2.2 -constraints jim {
        set cls [oo::class class {
            constructor foo { set _foo foo }
            method get-foo {} { return $_foo }
        }]

        try {
            set obj [${cls}::new x]
            $obj get-foo
        } finally {
            catch { namespace delete $obj }
            namespace delete $cls
        }
    } -> {can't read "_foo": no such variable}


    test list-mapv-1.1 {
        list::mapv -command list nothin' {} {}
    } -> {}

    test list-mapv-1.2 {
        list::mapv -command list x foo {y z} {bar baz qux} { list $x $y $z }
    } -> {{foo bar baz} {{} qux {}}}

    test list-mapv-1.3 {
        set acc {}
        set command [list append acc \n]

        list::mapv -command $command x foo {y z} {bar baz qux} {
            list $x $y $z
        }

        set acc
    } -> "\nx 0<1 {y z} 0<3\nx done {y z} 2<3\nx done {y z} done"


    test arity-1.1 {
        proc foo {} { arity 0 bar }

        try { foo } finally { rename foo {} }
    } -> {wrong # args: should be "foo bar"}

    test arity-1.2 {
        proc foo {a msg} {
            arity { $a <= 7 } $msg
        }

        try {
            foo 5 wrong
            foo 9 right
        } finally {
            rename foo {}
        }
    } -> {wrong # args: should be "foo right"}


    test template-parse-1.1 {
        set name {"Bob"}
        eval [template::parse {Hello, <%& $name %>}]
    } -> {Hello, &quot;Bob&quot;}


    test schema-validate-1.1 {
        set re {regexp {^[xy]$}}
        list \
            [schema::validate boolean yes] \
            [schema::validate boolean no] \
            [schema::validate boolean NO] \
            [schema::validate boolean Norway] \
            --- \
            [schema::validate $re x] \
            [schema::validate $re y] \
            [schema::validate $re xy] \

    } ->* {1 1 ? 0 --- 1 1 0}


    test schema-validate-dict-1.1 -constraints logical-negation-accepts-text {
        schema::validate::dict {
            foo boolean false
            absent {string length} placeholder
            present {string length} something
        } {foo true present 1 unmentioned {}}
    } -> {{foo true present 1 unmentioned {} absent placeholder} absent}


    return $stats
}


# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    puts [lib::run-tests]
}

package provide jimlib 0
