# Jimlib

![GitLab CI pipeline](https://gitlab.com/dbohdan/jimlib/badges/master/pipeline.svg)

Jimlib is my personal standard library for [Jim Tcl](https://jim.tcl-lang.org/).
It contains replacements for commands from Tcl 8–9 and Tcllib like `lib::clock::add` and `lib::try` and original functionality like `lib::named-args` and `lib::pipe` (a personal favorite).
It is oriented towards using Jim Tcl for shell scripting.
Most of the commands depend on few or no others.

The library is partially compatible with Tcl 8–9.
The test suite runs and passes in Tcl 8.6 and 9.0b2, although many tests are skipped.

Jimlib is not affiliated with the Jim Tcl project.
The API is unstable and may change at any time.

## Testing

Install Jim Tcl, Tcl 8.6 or 9, and [just](https://github.com/casey/just).
Run the command `just`.

## License

MIT, Tcl, and BSD.
See the top of `jimlib.tcl`.
