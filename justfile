force-color := "0"
jimsh := "jimsh"
jimlib-file := "jimlib.tcl"
tclsh := "tclsh"

default: test

test: jim tcl

jim:
  {{jimsh}} {{jimlib-file}}

tcl:
  {{tclsh}} {{jimlib-file}}

commands:
  {{jimsh}} procs.tcl procs {{jimlib-file}} {{force-color}}

commands-styles:
  {{jimsh}} procs.tcl styles
