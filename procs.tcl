#! /usr/bin/env jimsh

lappend auto_path [file dirname [info script]]
package require jimlib

set palette [lmap n {
    74 110 117 153 195 231
    136 172 214 220 222 229
    33 69 105 141 177 213
} { list fg 8-bit $n }]


proc style-ns-path {palette nsPath} {
    set color -1
    lib::pipe x {
        set nsPath
    } {
        lib::string::sepsplit $x :: 0
    } {
        set parts {}
        foreach word $x {
            lappend parts [style-word $palette $word]
        }
        set parts
    } {
        join $x [attrs faint]::[attrs]
    }
}


proc style-word {palette word} {
    set style {}

    if {$word eq {black}} {
        lappend style {fg 8-bit 238}
    } elseif {$word in {blue cyan default green magenta red white yellow}} {
        lappend style [list fg $word]
    } elseif {$word in {
        bold
        faint
        italic
        underline
        blink
        reverse
        strikethrough
        overline
    }} {
        lappend style {fg white} $word
    } else {
        lappend style [lindex $palette [expr {
            [lib::hash::mult $word 3] % [llength $palette]
        }]]
    }

    return [attrs {*}$style]$word[attrs]
}


proc procs {palette script {forceColor 0}} {
    if {$forceColor || [stdout isatty]} {
        local proc attrs args {lib::term::ansi::attrs {*}$args}
    } else {
        local proc attrs args {}
    }

    set lines {}
    foreach cmd [lib::commands $script] {
        if {[lindex $cmd 0] ni {proc lib::nsproc}} continue

        set line {}
        append line "[attrs faint][lindex $cmd 0][attrs] "
        append line "[style-ns-path $palette [lindex $cmd 1]][attrs] "
        append line [lrange $cmd 2 2]
        if {[llength $cmd] == 5} {
            append line " [attrs faint][lindex $cmd 3][attrs] "
        }

        lappend lines $line
    }

    return [join $lines \n]
}


proc procs-file {palette file args} {
    tailcall procs $palette [lib::file::read $file] {*}$args
}


proc styles palette {
    set lines {}

    foreach style $palette {
        lappend lines "[lib::term::ansi::attrs $style]\u2588\u2588 ##\
                       [format %-12s $style]\
                       ## \u2588\u2588[lib::term::ansi::attrs]"
    }

    join $lines \n
}


proc main argv {
    set argv [list $::palette {*}[lassign $argv command]]
    set argc [llength $argv]

    puts [switch -- $command {
        procs {
            lib::arity { $argc in {2 3} } {path ?force-color?} procs
            procs-file {*}$argv
        }
        styles {
            lib::arity { $argc == 1 } {} styles
            styles {*}$argv
        }
        default {
            error [list unknown command: $command]
        }
    }]
}


# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    main $argv
}
